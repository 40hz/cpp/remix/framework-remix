#pragma once

#include <concepts>

namespace remix::hal {

template<class P>
concept isGpio = requires(P p) {
    {
        p.set()
    };
    {
        p.clear()
    };
    {
        p.toggle()
    };
    {
        p.isSet()
    } -> std::same_as<bool>;
};

template<class P>
concept isOutput = requires(P p) {
    {
        p.set()
    };
    {
        p.clear()
    };
    {
        p.toggle()
    };
};

template<class P>
concept isInout = requires(P p) {
    {
        p.isSet()
    } -> std::same_as<bool>;
};

} // namespace remix::hal
