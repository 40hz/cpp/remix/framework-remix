#pragma once

#include <concepts>
#include <cstddef>
#include <cstdint>

namespace remix::hal {

template<class F>
concept isFlash = requires(F f) {
    {
        u.read()
    } -> std::same_as<uint8_t>;
    {
        u.write()
    };
    {
        u.unlock()
    };
    {
        u.lock()
    };
};

}; // namespace remix::hal
