#pragma once

#include <concepts>
#include <cstdint>

namespace remix::hal {

template<class U>
concept isUsart = requires(U u) {
    {
        u.receive()
    } -> std::same_as<uint8_t>;
    {
        u.send()
    };
    {
        u.canReceive()
    } -> std::same_as<bool>;
    {
        u.canSend()
    } -> std::same_as<bool>;
};

template<class S>
concept hasBeginEnd = requires(S s) {
    {
        s.begin()
    };
    {
        s.end()
    };
};

}; // namespace remix::hal
