#pragma once

#include <string_view>

namespace remix {

typedef std::basic_string_view<uint8_t> bytes_view;

inline namespace literals {

bytes_view operator""_b(const char* c, unsigned int n)
{
    return bytes_view(reinterpret_cast<const uint8_t*>(c), n);
}

}

}
