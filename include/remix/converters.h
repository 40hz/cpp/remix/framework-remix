#pragma once

#include <algorithm>
#include <concepts>
#include <expected>
#include <span>

namespace remix::converters {

enum ParseError {
    Insufficient_Data,
    Invalid_Data,
    Invalid_Radix,
};

enum Radix {
    Binary = 2,
    Octal = 8,
    Decimal = 10,
    Hexadecimal = 16,
};

template<typename T>
concept arithmetic = std::is_arithmetic_v<T>;

template<arithmetic T>
auto parse(auto& it, auto end, Radix radix = Decimal) -> std::expected<T, ParseError>
{
    T r = 0;
    if (*it == '0') {
        ++it;
        if (it == end) {
            return r;
        }
        if (*it >= '0' && *it <= '7') {
            radix = Octal;
            ++it;
        } else {
            switch (*it) {
            case 'b':
            case 'B':
                radix = Binary;
                ++it;
                break;
            case 'x':
            case 'X':
                radix = Hexadecimal;
                ++it;
                break;
            default:
                return std::unexpected(Invalid_Radix);
            }
            if (it == end) {
                return std::unexpected(Insufficient_Data);
            }
        }
    }
    while (it != end && *it > ' ') {
        auto c = *it;
        r *= static_cast<T>(radix);
        if (radix != Hexadecimal || c <= '9') {
            if (c < '0' || c > ('0' + radix - 1)) {
                return std::unexpected(Invalid_Data);
            }
            r += static_cast<T>(c - '0');
        } else {
            if (c >= 'a' && c <= 'f') {
                r += static_cast<T>(c - 'a' + 10);
            } else if (c >= 'A' && c <= 'F') {
                r += static_cast<T>(c - 'A' + 10);
            } else {
                return std::unexpected(Invalid_Data);
            }
        }
        ++it;
    }
    return r;
}

}; // namespace remix::converters
