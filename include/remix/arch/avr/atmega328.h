#pragma once

#include <avr/io.h>

#include "peripherals/gpio.h"
#include "peripherals/spi.h"
#include "peripherals/usart.h"
