#pragma once

#include <avr/io.h>
#include <remix/hal/usart.h>
#include <stdint.h>

namespace remix::mcu {

struct Spi {
    enum ClockPolarity { IdleLow, IdleHigh };
    enum ClockPhase { CaptureOnFirst, CaptureOnSecond };

    uint32_t speed { 1'000'000 };
    ClockPolarity clockPolarity { IdleLow };
    ClockPhase clockPhase { CaptureOnFirst };
    bool isMaster { true };
    uint32_t clockSpeed { 16'000'000 };

    auto init() const -> void
    {
        auto d = divider();
        SPCR = (clockPolarity == IdleHigh ? (1 << CPOL) : 0)
            | (clockPhase == CaptureOnSecond ? (1 << CPHA) : 0) | (isMaster ? (1 << MSTR) : 0)
            | (1 << SPE) | (d & 0b11);
        if (d & 0b100) {
            SPCR |= SPI2X;
        }
    }

    auto transmit(uint8_t data) const -> uint8_t
    {
        tx(data);
        while (!sendComplete())
            ;
        return rx();
    }

    auto tx(uint8_t data) const -> void { SPDR = data; }
    auto rx() const -> uint8_t { return SPDR; }

    auto send(uint8_t data) const -> void { transmit(data); }
    auto receive() const -> uint8_t { return transmit(0); }
    auto sendComplete() const -> bool { return (SPSR & (1 << SPIF)) != 0; }

    template<hal::hasBeginEnd S>
    auto write(const S s) const -> void
    {
        for (auto c : s) {
            send(c);
            while (!sendComplete())
                ;
        }
    }

    auto divider() const -> uint8_t
    {
        auto d = clockSpeed / speed;
        if (d <= 2)
            return 0b100;
        if (d <= 4)
            return 0b001;
        if (d <= 8)
            return 0b101;
        if (d <= 16)
            return 0b010;
        if (d <= 32)
            return 0b110;
        if (d <= 64)
            return 0b111;
        if (d <= 128)
            return 0b011;
        return 0b011;
    }
};

}
