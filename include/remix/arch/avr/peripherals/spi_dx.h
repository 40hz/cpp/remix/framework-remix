#pragma once

#include <avr/io.h>
#include <remix/hal/usart.h>
#include <stdint.h>

namespace remix::mcu {

struct Spi {
    enum ClockPolarity { IdleLow, IdleHigh };
    enum ClockPhase { CaptureOnFirst, CaptureOnSecond };
    enum Instance { Spi_0 = 0, Spi_1 = 1 };

    Instance instance { Spi_0 };
    uint32_t speed { 1'000'000 };
    ClockPolarity clockPolarity { IdleLow };
    ClockPhase clockPhase { CaptureOnFirst };
    bool isMaster { true };
    uint32_t clockSpeed { 4'000'000 };

    auto init() const -> void
    {
        auto d = divider();
        reg()->CTRLB = SPI_SSD_bm | (clockPolarity == IdleHigh ? SPI_MODE_1_bm : 0)
            | (clockPhase == CaptureOnSecond ? SPI_MODE_0_bm : 0);
        reg()->CTRLA = SPI_MASTER_bm | ((d & (0b100 != 0)) ? SPI_CLK2X_bm : 0)
            | (((d & 0b011) << 1) & 0b11) | SPI_ENABLE_bm;
    }

    auto transmit(uint8_t data) const -> uint8_t
    {
        tx(data);
        while (!sendComplete())
            ;
        return rx();
    }

    auto tx(uint8_t data) const -> void { reg()->DATA = data; }
    auto rx() const -> uint8_t { return reg()->DATA; }

    auto send(uint8_t data) const -> void { transmit(data); }
    auto receive() const -> uint8_t { return transmit(0); }
    auto sendComplete() const -> bool { return (reg()->INTFLAGS & SPI_IF_bm) != 0; }

    template<hal::hasBeginEnd S>
    auto write(const S s) const -> void
    {
        for (auto c : s) {
            send(c);
        }
    }

    auto divider() const -> uint8_t
    {
        auto d = clockSpeed / speed;
        if (d <= 2)
            return 0b100;
        if (d <= 4)
            return 0b001;
        if (d <= 8)
            return 0b101;
        if (d <= 16)
            return 0b010;
        if (d <= 32)
            return 0b110;
        if (d <= 64)
            return 0b111;
        if (d <= 128)
            return 0b011;
        return 0b011;
    }

    inline auto reg() const -> SPI_t*
    {
        static SPI_t* const periph[] = {
#ifdef SPI0
            &SPI0,
#else
            0,
#endif
#ifdef SPI1
            &SPI1,
#else
            0,
#endif
        };
        return periph[instance];
    }
};

}
