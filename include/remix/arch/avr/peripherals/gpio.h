#pragma once

#include <avr/io.h>
#include <stdint.h>

namespace remix::mcu {

struct Pin {
    enum Port { Port_B = 0, Port_C = 1, Port_D = 2 };
    enum Direction { Input, Output };

    const Port port;
    const uint8_t pin;
    const Direction direction = Input;

    auto init() const -> void
    {
        if (direction == Output) {
            *DDR() |= (1 << pin);
        } else {
            *DDR() &= (0xff ^ (1 << pin));
        }
    }

    auto set() const -> void { *PORT() |= (1 << pin); }
    auto clear() const -> void { *PORT() &= (0xff ^ (1 << pin)); }
    auto toggle() const -> void { *PIN() |= (1 << pin); }
    auto isSet() const -> bool { return (*PIN() & (0xff ^ (1 << pin))) != 0; }

    inline auto PORT() const -> volatile uint8_t*
    {
        static volatile uint8_t* const registers[] = { &PORTB, &PORTC, &PORTD };
        return registers[port];
    }

    inline auto DDR() const -> volatile uint8_t*
    {
        static volatile uint8_t* const registers[] = { &DDRB, &DDRC, &DDRD };
        return registers[port];
    }

    inline auto PIN() const -> volatile uint8_t*
    {
        static volatile uint8_t* const registers[] = { &PINB, &PINC, &PIND };
        return registers[port];
    }
};
}
