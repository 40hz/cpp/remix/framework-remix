#pragma once

#include <avr/io.h>

#include <remix/hal/usart.h>
#include <remix/timeouts.h>

namespace remix::mcu {

struct Usart {
    auto init() const -> void
    {
        UBRR0H = 0;
        UBRR0L = 16;
        UCSR0A = 1 << U2X0;
        UCSR0B = (1 << TXEN0) | (1 << RXEN0);
        UCSR0C = (1 << UCSZ00) | (1 << UCSZ01);
    }

    template<typename T = timeouts::Never>
    auto send(uint8_t data, T timeout = T {}) const -> void
    {
        while (!timeout.timedOut() && !canSend())
            ;
        tx(data);
    }

    template<typename T = timeouts::Never>
    auto receive(T timeout = T {}) const -> uint8_t
    {
        while (!timeout.timedOut() && !canReceive())
            ;
        return rx();
    }

    template<hal::hasBeginEnd S>
    auto write(const S s) const -> void
    {
        for (auto c : s) {
            send(c);
        }
    }

    auto tx(uint8_t data) const -> void { UDR0 = data; }
    auto rx() const -> uint8_t { return UDR0; }

    auto canSend() const -> bool { return (UCSR0A & (1 << UDRE0)) != 0; }
    auto canReceive() const -> bool { return (UCSR0A & (1 << RXC0)) != 0; }
};

}
