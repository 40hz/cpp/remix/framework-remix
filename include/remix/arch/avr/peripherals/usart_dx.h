#pragma once

#include <avr/io.h>
#include <remix/hal/usart.h>
#include <remix/timeouts.h>
#include <stdint.h>

namespace remix::mcu {

struct Usart {
    enum Instance {
        Usart_0 = 0,
        Usart_1 = 1,
        Usart_2 = 2,
        Usart_3 = 3,
        Usart_4 = 4,
        Usart_5 = 5,
    };

    Instance instance { Usart_0 };
    uint32_t speed { 115'200 };
    uint32_t clockSpeed { 4'000'000 };

    auto init() const -> void
    {
        reg()->BAUD = (4 * clockSpeed / speed) & 0xff;
        reg()->CTRLC = USART_CHSIZE_0_bm | USART_CHSIZE_1_bm | USART_CHSIZE_2_bm;
        reg()->CTRLB = USART_TXEN_bm | USART_RXEN_bm;
    }

    template<typename T = timeouts::Never>
    auto send(uint8_t data, T timeout = T {}) const -> void
    {
        while (!timeout.timedOut() && !canSend())
            ;
        tx(data);
    }

    template<typename T = timeouts::Never>
    auto receive(T timeout = T {}) const -> uint8_t
    {
        while (!timeout.timedOut() && !canReceive())
            ;
        return rx();
    }

    template<hal::hasBeginEnd S>
    auto write(const S s) const -> void
    {
        for (auto c : s) {
            send(c);
        }
    }

    auto tx(uint8_t data) const -> void { reg()->TXDATAL = data; }
    auto rx() const -> uint8_t { return reg()->RXDATAL; }

    auto canSend() const -> bool { return (reg()->STATUS & USART_DREIF_bm) != 0; }
    auto canReceive() const -> bool { return (reg()->STATUS & USART_RXCIF_bm) != 0; }

    inline auto reg() const -> USART_t*
    {
        static USART_t* const periph[] = {
#ifdef USART0
            &USART0,
#else
            0,
#endif
#ifdef USART1
            &USART1,
#else
            0,
#endif
#ifdef USART2
            &USART2,
#else
            0,
#endif
#ifdef USART3
            &USART3,
#else
            0,
#endif
#ifdef USART4
            &USART4,
#else
            0,
#endif
#ifdef USART5
            &USART5,
#else
            0,
#endif
        };
        return periph[instance];
    }
};

}

#if 0
    return struct {
        pub fn init() void {
            usart.BAUD = 4 * 4_000_000 / 115200;
            usart.CTRLC.write(.{
                .CMODE = .{ .value = .ASYNCHRONOUS },
                .PMODE = .{ .value = .DISABLED },
                .SBMODE = .{ .value = .@"1BIT" },
                .CHSIZE = .{ .value = .@"8BIT" },
            });
            usart.CTRLB.modify(.{ .RXEN = 1, .TXEN = 1 });
        }

        pub fn dtr() bool {
            return true;
        }

        pub fn rxAvailable() bool {
            return usart.STATUS.read().RXCIF == 1;
        }

        pub fn send(c: u8) void {
            while (usart.STATUS.read().DREIF == 0) {}
            usart.TXDATAL.write(.{ .DATA = c });
        }

        pub fn receive() u8 {
            while (usart.STATUS.read().RXCIF == 0) {}
            return usart.RXDATAL.read().DATA;
        }

#endif
