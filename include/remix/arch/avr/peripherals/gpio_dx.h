#pragma once

#include <avr/io.h>
#include <stdint.h>

namespace remix::mcu {

struct Pin {
    enum Port { Port_A = 0, Port_B = 1, Port_C = 2, Port_D = 3, Port_E = 4, Port_F = 5 };
    enum Direction { Input, Output };

    const Port port;
    const uint8_t pin;
    const Direction direction = Input;
    const bool pullup { false };

    auto init() const -> void
    {
        if (direction == Output) {
            reg()->DIRSET = (1 << pin) & 0xff;
        } else {
            reg()->DIRCLR = (1 << pin) & 0xff;
        }
        if (pullup) {
            switch (pin) {
            case 0:
                reg()->PIN0CTRL |= PORT_PULLUPEN_bm;
                break;
            case 1:
                reg()->PIN1CTRL |= PORT_PULLUPEN_bm;
                break;
            case 2:
                reg()->PIN2CTRL |= PORT_PULLUPEN_bm;
                break;
            case 3:
                reg()->PIN3CTRL |= PORT_PULLUPEN_bm;
                break;
            case 4:
                reg()->PIN4CTRL |= PORT_PULLUPEN_bm;
                break;
            case 5:
                reg()->PIN5CTRL |= PORT_PULLUPEN_bm;
                break;
            case 6:
                reg()->PIN6CTRL |= PORT_PULLUPEN_bm;
                break;
            case 7:
                reg()->PIN7CTRL |= PORT_PULLUPEN_bm;
                break;
            }
        }
    }

    auto set() const -> void { reg()->OUTSET = (1 << pin) & 0xff; }
    auto clear() const -> void { reg()->OUTCLR = (1 << pin) & 0xff; }
    auto toggle() const -> void { reg()->OUTTGL = (1 << pin) & 0xff; }
    auto isSet() const -> bool { return (reg()->IN & (1 << pin)) != 0; }

    inline auto reg() const -> PORT_t*
    {
        static PORT_t* const ports[] = {
#ifdef PORTA
            &PORTA,
#else
            0,
#endif
#ifdef PORTB
            &PORTB,
#else
            0,
#endif
#ifdef PORTC
            &PORTC,
#else
            0,
#endif
#ifdef PORTD
            &PORTD,
#else
            0,
#endif
#ifdef PORTE
            &PORTE,
#else
            0,
#endif
#ifdef PORTF
            &PORTF,
#else
            0,
#endif
#ifdef PORTG
            &PORTG,
#else
            0,
#endif
        };
        return ports[port];
    }
};

}
