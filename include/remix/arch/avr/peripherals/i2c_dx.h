#pragma once

#include <avr/io.h>
#include <remix/timeouts.h>
#include <span>
#include <stdint.h>
#include <string_view>

namespace remix::mcu {

struct I2c {
    enum Instance { Twi_0 = 0, Twi_1 = 1 };
    enum Direction { Read, Write };

    const Instance instance { Twi_0 };
    const uint32_t speed { 100'000 };
    const uint32_t clockSpeed { 4'000'000 };

    auto init() const -> void
    {
        reg()->MBAUD = (clockSpeed / speed / 2) & 0xff;
        reg()->MCTRLA |= TWI_ENABLE_bm;
        busIdle();
    }

    template<typename T = timeouts::Never>
    auto start(uint8_t address, Direction direction = Write, T timeout = T {}) const -> bool
    {
        reg()->MADDR = ((address << 1) | (direction == Write ? 0 : 1)) & 0xff;
        if (!waitForInterrupt(direction == Write ? TWI_WIF_bm : TWI_RIF_bm, timeout)) {
            return false;
        }
        if ((reg()->MSTATUS & TWI_RXACK_bm) != 0) {
            generateStop();
            return false;
        }
        return true;
    }

    template<typename T = timeouts::Never>
    auto checkTarget(uint8_t address, T timeout = T {}) const -> bool
    {
        return start(address, Write, timeout);
    }

    template<typename T = timeouts::Never>
    auto send(uint8_t d, T timeout = T {}) const -> bool
    {
        reg()->MDATA = d;
        if (!waitForInterrupt(TWI_WIF_bm, timeout)) {
            return false;
        }
        return true;
    }

    template<typename T = timeouts::Never>
    auto controllerSend(uint8_t address, std::basic_string_view<uint8_t> data, bool restart = false,
        T timeout = T {}) const -> bool
    {
        if (!start(address, Write, timeout)) {
            return false;
        }
        for (auto const& d : data) {
            if (!send(d, timeout)) {
                return false;
            }
            if ((reg()->MSTATUS & TWI_RXACK_bm) != 0) {
                break;
            }
        }
        if (!restart) {
            generateStop();
        }
        return true;
    }

    template<typename T = timeouts::Never>
    auto controllerReceive(uint8_t address, std::span<uint8_t> data, T timeout = T {}) const -> bool
    {
        if (!start(address, Read, timeout)) {
            return false;
        }
        size_t i = 0;
        for (auto& d : data) {
            d = reg()->MDATA;
            if (i < data.size() - 1) {
                reg()->MCTRLB = TWI_MCMD_RECVTRANS_gc;
                if (!waitForInterrupt(TWI_RIF_bm, timeout)) {
                    return false;
                }
            } else {
                reg()->MCTRLB = TWI_ACKACT_bm | TWI_MCMD_STOP_gc;
            }
            i += 1;
        }
        return true;
    }

    template<typename T = timeouts::Never>
    auto writeRegister(uint8_t address, uint8_t r, std::basic_string_view<uint8_t> data,
        T timeout = {}) const -> bool
    {
        if (!start(address, Write, timeout)) {
            return false;
        }
        if (!send(r, timeout)) {
            return false;
        }
        return controllerSend(address, data, timeout);
    }

    template<typename T = timeouts::Never>
    auto readRegister(uint8_t address, uint8_t r, std::span<uint8_t> data, T timeout = {}) const
        -> bool
    {
        if (!start(address, Write, timeout)) {
            return false;
        }
        if (!send(r, timeout)) {
            return false;
        }
        return controllerReceive(address, data, timeout);
    }

    template<typename T = timeouts::Never>
    auto waitForInterrupt(uint8_t interrupt, T timeout = T {}) const -> bool
    {
        while (!timeout.timedOut()) {
            if ((reg()->MSTATUS & interrupt) != 0) {
                return true;
            }
        }
        if ((reg()->MSTATUS & TWI_ARBLOST_bm) != 0) {
            return false;
        }
        generateStop();
        return false;
    }

    auto generateStop() const -> void
    {
        reg()->MCTRLB = TWI_MCMD_STOP_gc;
        busIdle();
    }

    auto busIdle() const -> void { reg()->MSTATUS |= TWI_BUSSTATE_IDLE_gc; }

    inline auto reg() const -> TWI_t*
    {
        static TWI_t* const periph[] = {
#ifdef TWI0
            &TWI0,
#else
            0,
#endif
#ifdef TWI1
            &TWI1,
#else
            0,
#endif
        };
        return periph[instance];
    }
};

}
