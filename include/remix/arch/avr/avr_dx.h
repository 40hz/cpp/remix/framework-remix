#pragma once

#include <avr/io.h>

#include "peripherals/gpio_dx.h"
#include "peripherals/i2c_dx.h"
#include "peripherals/spi_dx.h"
#include "peripherals/usart_dx.h"
