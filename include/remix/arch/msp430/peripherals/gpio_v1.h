#pragma once

#include <stdint.h>

#include <remix/hal/pin.h>
#include <remix/memptr.h>

namespace remix::mcu {

class Pin {
public:
    enum PullMode { No_Pull, Pull_Up, Pull_Down };
    enum AlternateFunction { None, Primary, Secondary };
    enum Direction { Input, Output };

    struct Registers {
        volatile uint8_t* in;
        volatile uint8_t* out;
        volatile uint8_t* dir;
        volatile uint8_t* ifg;
        volatile uint8_t* ies;
        volatile uint8_t* ie;
        volatile uint8_t* sel;
        volatile uint8_t* sel2;
        volatile uint8_t* ren;
    };

    uint8_t port;
    uint8_t pin;
    PullMode pull { No_Pull };
    Direction direction { Input };
    AlternateFunction function { None };

    auto init() const -> void
    {
        switch (direction) {
        case Input:
            *reg().dir &= static_cast<uint8_t>(~(1 << pin));
            break;
        case Output:
            *reg().dir |= (1 << pin);
            break;
        }
        switch (pull) {
        case Pull_Up:
            *reg().ren |= 1 << pin;
            *reg().out |= 1 << pin;
            break;
        case Pull_Down:
            *reg().ren |= 1 << pin;
            *reg().out &= static_cast<uint8_t>(~(1 << pin));
            break;
        case No_Pull:
            *reg().ren &= static_cast<uint8_t>(~(1 << pin));
            break;
        }
        switch (function) {
        case Primary:
            *reg().sel |= 1 << pin;
            *reg().sel2 &= static_cast<uint8_t>(~(1 << pin));
            break;
        case Secondary:
            *reg().sel |= 1 << pin;
            *reg().sel2 |= 1 << pin;
            break;
        case None:
            *reg().sel &= static_cast<uint8_t>(~(1 << pin));
            *reg().sel2 &= static_cast<uint8_t>(~(1 << pin));
            break;
        }
    }

    auto set() const -> void { *reg().out |= (1 << pin); }
    auto clear() const -> void { *reg().out &= static_cast<uint8_t>(~(1 << pin)); }
    auto toggle() const -> void { *reg().out ^= (1 << pin); }
    auto isSet() const -> bool { return *reg().in & (1 << pin); }

protected:
    static constexpr Registers registers[] = {
        { &P1IN, &P1OUT, &P1DIR, &P1IFG, &P1IES, &P1IE, &P1SEL, &P1SEL2, &P1REN },
        { &P2IN, &P2OUT, &P2DIR, &P2IFG, &P2IES, &P2IE, &P2SEL, &P2SEL2, &P2REN },
    };

    inline auto reg() const -> const Registers& { return registers[port]; }
};

}; // namespace remix::mcu
