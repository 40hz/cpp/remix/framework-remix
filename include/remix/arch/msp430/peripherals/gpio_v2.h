#pragma once

#include <stdint.h>

#include <remix/hal/pin.h>
#include <remix/memptr.h>

namespace remix::mcu {

class Pin {
public:
    enum PullMode { No_Pull, Pull_Up, Pull_Down };
    enum Direction { Input, Output };

    struct Registers {
        volatile uint8_t* in;
        volatile uint8_t* out;
        volatile uint8_t* dir;
        volatile uint8_t* ifg;
        volatile uint8_t* ies;
        volatile uint8_t* ie;
        volatile uint8_t* sel;
        volatile uint8_t* ren;
    };

    uint8_t port;
    uint8_t pin;
    PullMode pull { No_Pull };
    Direction direction { Input };
    bool alt { false };

    auto init() const -> void
    {
        switch (direction) {
        case Input:
            *reg().dir &= static_cast<uint8_t>(~(1 << pin));
            break;
        case Output:
            *reg().dir |= (1 << pin);
            break;
        }
        switch (pull) {
        case Pull_Up:
            *reg().ren |= 1 << pin;
            *reg().out |= 1 << pin;
            break;
        case Pull_Down:
            *reg().ren |= 1 << pin;
            *reg().out &= static_cast<uint8_t>(~(1 << pin));
            break;
        case No_Pull:
            *reg().ren &= static_cast<uint8_t>(~(1 << pin));
            break;
        }
        if (alt) {
            *reg().sel |= 1 << pin;
        } else {
            *reg().sel &= static_cast<uint8_t>(~(1 << pin));
        }
    }

    auto set() const -> void { *reg().out |= (1 << pin); }
    auto clear() const -> void { *reg().out &= static_cast<uint8_t>(~(1 << pin)); }
    auto toggle() const -> void { *reg().out ^= (1 << pin); }
    auto isSet() const -> bool { return *reg().in & (1 << pin); }

protected:
    static constexpr Registers registers[] = {
        { &P1IN, &P1OUT, &P1DIR, &P1IFG, &P1IES, &P1IE, &P1SEL0, &P1REN },
        { &P2IN, &P2OUT, &P2DIR, &P2IFG, &P2IES, &P2IE, &P2SEL0, &P2REN },
        { &P3IN, &P3OUT, &P3DIR, 0, 0, 0, &P3SEL0, &P3REN },
        { &P4IN, &P4OUT, &P4DIR, 0, 0, 0, &P4SEL0, &P4REN },
        { &P5IN, &P5OUT, &P5DIR, 0, 0, 0, &P5SEL0, &P5REN },
        { &P6IN, &P6OUT, &P6DIR, 0, 0, 0, &P6SEL0, &P6REN },
        { &P7IN, &P7OUT, &P7DIR, 0, 0, 0, &P7SEL0, &P7REN },
        { &P8IN, &P8OUT, &P8DIR, 0, 0, 0, &P8SEL0, &P8REN },
    };

    inline auto reg() const -> const Registers& { return registers[port]; }
};

}; // namespace remix::mcu
