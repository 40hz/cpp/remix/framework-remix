#pragma once

#include <cstdint>

namespace remix::mcu {

class ClockSystem {
public:
    struct Clock {
        enum ClockSource { VloClk, Xt1Clk, RefoClk, DcoClk, ModClk };
        enum ClockType { AClk, MClk, SmClk };

        ClockType type;
        ClockSource source;
        uint32_t frequency;
    };

    Clock mClock { .type = Clock::MClk, .source = Clock::DcoClk, .frequency = 1'000'000 };
    Clock smClock { .type = Clock::SmClk, .source = Clock::DcoClk, .frequency = 1'000'000 };
    Clock aClock { .type = Clock::AClk, .source = Clock::VloClk, .frequency = 32'768 };

    auto init() const -> void
    {
        CSCTL0 = 0;
        switch (mClock.frequency) {
        default:
        case 1'000'000:
            CSCTL1 = DCORSEL_0;
            CSCTL2 = 1'000'000 / 32768 - 1;
            break;
        case 2'000'000:
            CSCTL1 = DCORSEL_1;
            CSCTL2 = 2'000'000 / 32768 - 1;
            break;
        case 4'000'000:
            CSCTL1 = DCORSEL_2;
            CSCTL2 = 4'000'000 / 32768 - 1;
            break;
        case 8'000'000:
            CSCTL1 = DCORSEL_3;
            CSCTL2 = 8'000'000 / 32768 - 1;
            break;
        case 12'000'000:
            CSCTL1 = DCORSEL_4;
            CSCTL2 = 12'000'000 / 32768 - 1;
            break;
        case 16'000'000:
            CSCTL1 = DCORSEL_5;
            CSCTL2 = 16'000'000 / 32768 - 1;
            break;
        }
        if (mClock.frequency == 16'000'000) {
            FRCTL0 = FRCTLPW | NWAITS0;
            FRCTL0_H = 0;
        } else {
            FRCTL0 = FRCTLPW;
            FRCTL0_H = 0;
        }
        CSCTL3 = SELREF__REFOCLK;
        CSCTL4 = SELA__XT1CLK | SELMS__DCOCLKDIV;
        CSCTL7 &= ~(0x0004 | XT1OFFG | DCOFFG);
        enableXtal();
    }

    auto enableXtal() const -> void
    {
        uint16_t timeout = 64;
        do {
            timeout--;
            CSCTL7 &= ~(DCOFFG | XT1OFFG | FLLULIFG);
            SFRIFG1 &= ~OFIFG;
            __delay_cycles(500000L);
            if (!timeout)
                break;
        } while (SFRIFG1 & OFIFG);

        if (!timeout) {
            CSCTL4 |= SELA__REFOCLK;
        }
    }
};

} // namespace remix::mcu
