#pragma once

#include <cstdint>

#include <remix/timeouts.h>

#include "basic_clock.h"

namespace remix::mcu {

class Uart {
public:
    uint32_t speed { 9600 };
    const ClockModule::Clock& clock;

    auto init() const -> void
    {
        UCA0CTL1 |= UCSWRST;
        UCA0CTL0 &= ~(UCMODE1 | UCMODE0);
        switch (clock.type) {
        case ClockModule::Clock::SmClk:
            UCA0CTL1 = (UCA0CTL1 & ~UCSSEL0) | UCSSEL1;
            break;
        default:
            break;
        }
        uint32_t brr = clock.frequency / speed;
        UCA0BR0 = brr & 0xff;
        UCA0BR1 = (brr >> 8) & 0xff;
        UCA0CTL1 &= ~UCSWRST;
    };

    template<typename T = timeouts::Never>
    auto send(uint8_t data, T timeout = T {}) const -> void
    {
        while (!timeout.timedOut() && !canSend())
            ;
        UCA0TXBUF = data;
    }
    template<typename T = timeouts::Never>
    auto receive(T timeout = T {}) const -> uint8_t
    {
        while (!timeout.timedOut() && !canReceive())
            ;
        return UCA0RXBUF;
    }
    auto canSend() const -> bool { return (IFG2 & UCA0TXIFG) != 0; }
    auto canReceive() const -> bool { return (IFG2 & UCA0RXIFG) != 0; }

    template<typename S>
    auto write(const S s) const -> void
    {
        for (auto c : s) {
            send(c);
        }
    }
};

}; // namespace remix::mcu
