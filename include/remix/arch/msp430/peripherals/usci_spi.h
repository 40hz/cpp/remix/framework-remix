#pragma once

#include <cstdint>

#include "basic_clock.h"

namespace remix::mcu {

class Spi {
public:
    enum Module { USCI_A = 0, USCI_B = 1 };
    struct Registers {
        volatile uint8_t* ctl0;
        volatile uint8_t* ctl1;
        volatile uint8_t* br0;
        volatile uint8_t* br1;
        volatile uint8_t* txbuf;
        volatile uint8_t* rxbuf;
    };

    Module usci_module { USCI_A };
    uint32_t speed { 1'000'000 };
    uint8_t mode { 0 };
    const ClockModule::Clock& clock;

    auto init() const -> void
    {
        *reg().ctl1 |= UCSWRST;
        *reg().ctl0
            = ((mode & 0b01) ? 0 : UCCKPH) | ((mode & 0b10) ? UCCKPL : 0) | UCMST | UCSYNC | UCMSB;
        switch (clock.type) {
        case ClockModule::Clock::SmClk:
            *reg().ctl1 = (*reg().ctl1 & ~UCSSEL0) | UCSSEL1;
            break;
        default:
            break;
        }
        uint32_t brr = clock.frequency / speed;
        *reg().br0 = brr & 0xff;
        *reg().br1 = (brr >> 8) & 0xff;
        *reg().ctl1 &= ~UCSWRST;
    };

    auto transmit(uint8_t data) const -> uint8_t
    {
        while (!canSend())
            ;
        *reg().txbuf = data;
        while (!canReceive())
            ;
        return *reg().rxbuf;
    }
    auto send(uint8_t data) const -> void { transmit(data); }
    auto receive() const -> uint8_t { return transmit(0); }
    auto canSend() const -> bool
    {
        return (IFG2 & (usci_module == USCI_A ? UCA0TXIFG : UCB0TXIFG)) != 0;
    }
    auto canReceive() const -> bool
    {
        return (IFG2 & (usci_module == USCI_A ? UCA0RXIFG : UCB0RXIFG)) != 0;
    }

    template<typename S>
    auto write(const S s) const -> void
    {
        for (auto c : s) {
            while (!canSend())
                ;
            send(c);
        }
    }

protected:
    static constexpr Registers registers[2]
        = { { &UCA0CTL0, &UCA0CTL1, &UCA0BR0, &UCA0BR1, &UCA0TXBUF, &UCA0RXBUF },
              { &UCB0CTL0, &UCB0CTL1, &UCB0BR0, &UCB0BR1, &UCB0TXBUF, &UCB0RXBUF } };

    inline auto reg() const -> const Registers& { return registers[usci_module]; }
};

}; // namespace remix::mcu
