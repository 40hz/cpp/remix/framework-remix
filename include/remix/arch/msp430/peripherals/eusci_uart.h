#pragma once

#include <cstdint>

#include <remix/timeouts.h>

#include "cs_clock.h"

namespace remix::mcu {

class Uart {
public:
    uint32_t speed { 9600 };
    const ClockSystem::Clock& clock;

    auto init() const -> void
    {
        uint32_t brr = clock.frequency / speed;
        UCA0CTL1 |= UCSWRST;
        UCA0CTL0 &= ~(UCMODE1 | UCMODE0);
        switch (clock.type) {
        case ClockSystem::Clock::SmClk:
            UCA0CTL1 = (UCA0CTL1 & ~UCSSEL0) | UCSSEL1 | UCSWRST;
            break;
        default:
            break;
        }
        if (brr > 16) {
            UCA0MCTLW |= UCOS16 | ((brr % 16) << 4) | (0x0 << 8);
            UCA0BRW = (brr / 16) & 0xffff;
        } else {
            UCA0MCTLW = 0;
            UCA0BRW = brr & 0xffff;
        }
        UCA0CTL1 &= ~UCSWRST;
    };

    template<typename T = timeouts::Never>
    auto send(uint8_t data, T timeout = T {}) const -> void
    {
        while (!timeout.timedOut() && !canSend())
            ;
        UCA0TXBUF_L = data;
    }
    template<typename T = timeouts::Never>
    auto receive(T timeout = T {}) const -> uint8_t
    {
        while (!timeout.timedOut() && !canReceive())
            ;
        return UCA0RXBUF_L;
    }
    auto canSend() const -> bool { return (UCA0IFG & UCTXIFG) != 0; }
    auto canReceive() const -> bool { return (UCA0IFG & UCRXIFG) != 0; }

    template<typename S>
    auto write(const S s) const -> void
    {
        for (auto c : s) {
            send(c);
        }
    }
};

}; // namespace remix::mcu
