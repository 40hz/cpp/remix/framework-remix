#pragma once

#include <array>
#include <cstdint>

namespace remix::mcu {

class ClockModule {
public:
    struct Clock {
        enum ClockSource { VloClk, Lfxt1Clk, DcoClk };
        enum ClockType { AClk, MClk, SmClk };

        ClockType type;
        ClockSource source;
        uint32_t frequency;
    };

    Clock const mClock { .type = Clock::MClk, .source = Clock::DcoClk, .frequency = 1'000'000 };
    Clock const smClock { .type = Clock::SmClk, .source = Clock::DcoClk, .frequency = 1'000'000 };
    Clock const aClock { .type = Clock::AClk, .source = Clock::VloClk, .frequency = 32'768 };

    auto init() const -> void
    {
        switch (mClock.frequency) {
        case 8'000'000:
            BCSCTL1 = CALBC1_8MHZ;
            DCOCTL = CALDCO_8MHZ;
            break;
        case 12'000'000:
            BCSCTL1 = CALBC1_12MHZ;
            DCOCTL = CALDCO_12MHZ;
            break;
        case 16'000'000:
            BCSCTL1 = CALBC1_16MHZ;
            DCOCTL = CALDCO_16MHZ;
            break;
        default:
        case 1'000'000:
            BCSCTL1 = CALBC1_1MHZ;
            DCOCTL = CALDCO_1MHZ;
            break;
        }
    }
};

} // namespace remix::mcu
