#pragma once

#include <cstdint>

#include "cs_clock.h"

namespace remix::mcu {

class Spi {
public:
    enum Module { eUSCI_A = 0, eUSCI_B = 1 };

    struct Registers {
        volatile unsigned int* ctlw;
        volatile unsigned int* brw;
        volatile uint8_t* statw;
        volatile unsigned int* txbuf;
        volatile unsigned int* rxbuf;
        volatile uint8_t* txbufb;
        volatile uint8_t* rxbufb;
        volatile unsigned int* ie;
        volatile unsigned int* ifg;
        volatile unsigned int* iv;
    };

    Module eusci_module { eUSCI_A };
    uint8_t instance { 0 };
    uint32_t speed { 1'000'000 };
    uint8_t mode { 0 };
    const ClockSystem::Clock& clock;

    auto init() const -> void
    {
        *reg().ctlw |= UCSWRST;
        *reg().ctlw = UCSWRST | ((mode & 0b01) ? 0 : UCCKPH) | ((mode & 0b10) ? UCCKPL : 0) | UCMST
            | UCSYNC | UCMSB | UCSSEL1;
        *reg().brw = (clock.frequency / speed) & 0xffff;
        *reg().ctlw &= ~UCSWRST;
    };

    auto transmit(uint8_t data) const -> uint8_t
    {
        while (!canSend())
            ;
        *reg().txbufb = data;
        while (!canReceive())
            ;
        return *reg().rxbufb;
    }
    auto send(uint8_t data) const -> void { transmit(data); }
    auto receive() const -> uint8_t { return transmit(0); }
    auto canSend() const -> bool { return (*reg().ifg & UCTXIFG); }
    auto canReceive() const -> bool { return (*reg().ifg & UCRXIFG); }

    template<typename S>
    auto write(const S s) const -> void
    {
        for (auto c : s) {
            while (!canSend())
                ;
            send(c);
        }
    }

protected:
    static constexpr Registers registers[2][10] = {
        {
            { &UCA0CTLW0, &UCA0BRW, &UCA0STATW, &UCA0TXBUF, &UCA0RXBUF, &UCA0TXBUF_L, &UCA0RXBUF_L,
                &UCA0IE, &UCA0IFG, &UCA0IV },
        },
        {
            { &UCB0CTLW0, &UCB0BRW, &UCB0STATW_L, &UCB0TXBUF, &UCB0RXBUF, &UCB0TXBUF_L,
                &UCB0RXBUF_L, &UCB0IE, &UCB0IFG, &UCB0IV },
        },
    };

    inline auto reg() const -> const Registers& { return registers[eusci_module][instance]; }
};

}; // namespace remix::mcu
