#pragma once

#include <span>

#include <remix/flash_region.h>

namespace remix::mcu {

template<auto regions, typename Clock>
class Flash {
public:
    enum Status {
        Ok,
        Invalid_Address,
        Flash_Locked,
        Access_Violation,
    };

    const Clock& clock;

    auto init() const -> void
    {
        const uint8_t divider = calculateDivider(1, clock.frequency) - 1;
        switch (clock.type) {
        case Clock::AClk:
            FCTL2 = FWKEY | divider;
            break;
        case Clock::MClk:
            FCTL2 = FWKEY | FSSEL_1 | divider;
            break;
        case Clock::SmClk:
            FCTL2 = FWKEY | FSSEL_2 | divider;
            break;
        }
    }

    template<typename T>
    auto write(size_t addr, T* t) const -> Status
    {
        auto region = addrToRegion(addr, regions);
        if (!region) {
            return Invalid_Address;
        }
        FCTL1 = FWKEY + WRT;
        uint16_t const* src = reinterpret_cast<uint16_t const* const>(t);
        uint16_t* dst = reinterpret_cast<uint16_t*>(addr);
        for (size_t i = 0; i < (sizeof(T) + 1) / 2; i++) {
            *dst = *src;
            ++dst;
            ++src;
        }
        return Ok;
    }

    auto erase(size_t addr) const -> Status
    {
        auto region = addrToRegion(addr, regions);
        if (!region) {
            return Invalid_Address;
        }
        while (FCTL3 & BUSY)
            ;
        FCTL1 = FWKEY + ERASE;
        *reinterpret_cast<uint16_t*>(addr) = 0;
        __delay_cycles(1000);
        return Ok;
    }

    auto unlock() const -> void
    {
        while (FCTL3 & BUSY)
            ;
        FCTL3 = FWKEY;
    }
    auto lock() const -> void { FCTL3 = FWKEY + LOCK; }

    static constexpr auto ftgOk(const uint8_t divider, const uint32_t frequency) -> bool
    {
        return frequency / (unsigned long)divider > 257000L
            && frequency / (unsigned long)divider < 476000L;
    }

    static constexpr auto calculateDivider_(const uint8_t divider, const uint32_t frequency)
        -> uint8_t
    {
        return divider <= 64
            ? (ftgOk(divider, frequency) ? divider : calculateDivider(divider + 1, frequency))
            : 255;
    }

    static constexpr auto calculateDivider(
        [[maybe_unused]] const uint8_t divider, const uint32_t frequency) -> uint8_t
    {
        if (frequency >= 16'000'000) {
            return 40;
        }
        if (frequency >= 12'000'000) {
            return 30;
        }
        if (frequency >= 8'000'000) {
            return 20;
        }
        return 3;
    }
};
}; // namespace remix::mcu
