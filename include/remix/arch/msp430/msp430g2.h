#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#include "msp430.h"
#pragma GCC diagnostic pop

#undef N
#undef Z

#include <remix/flash_region.h>

#include "peripherals/basic_clock.h"
#include "peripherals/flash_v1.h"
#include "peripherals/gpio_v1.h"
#include "peripherals/usci_spi.h"
#include "peripherals/usci_uart.h"
