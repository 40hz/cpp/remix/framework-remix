#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#include "msp430.h"
#pragma GCC diagnostic pop

#undef C
#undef N

#include "peripherals/cs_clock.h"
#include "peripherals/eusci_spi.h"
#include "peripherals/eusci_uart.h"
#include "peripherals/gpio_v2.h"
