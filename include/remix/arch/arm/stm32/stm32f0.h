#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wvolatile"
#include "stm32f0xx.h"
#pragma GCC diagnostic pop
#include "peripherals/gpio_v2.h"
#include "peripherals/i2c_v2.h"
#include "peripherals/spi_v2.h"
#include "peripherals/usart_v2.h"
