#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wvolatile"
#include "stm32f1xx.h"
#pragma GCC diagnostic pop
#include "peripherals/gpio_v1.h"
