#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wvolatile"
#include "stm32l0xx.h"
#pragma GCC diagnostic pop
#include "peripherals/flash_v2.h"
#include "peripherals/gpio_v2.h"
#include "peripherals/i2c_v2.h"
#include "peripherals/spi_v3.h"
#include "peripherals/usart_v2.h"
