#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wvolatile"
#include "stm32f3xx.h"
#pragma GCC diagnostic pop
#include "peripherals/flash_v1.h"
#include "peripherals/gpio_v2.h"
#include "peripherals/spi_v2.h"
#include "peripherals/usart_v2.h"
