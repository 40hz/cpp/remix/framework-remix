#pragma once

#include <remix/flash_region.h>

namespace remix::mcu {

template<auto regions>
class Flash {
public:
    enum Status {
        Ok,
        Invalid_Address,
        Flash_Locked,
        Access_Violation,
    };

    auto init() const -> void {}

    template<typename T>
    auto write(size_t addr, T* t) const -> Status
    {
        auto region = addrToRegion(addr, regions);
        if (!region) {
            return Invalid_Address;
        }
        FLASH->CR &= ~(FLASH_CR_PER | FLASH_CR_MER);
        FLASH->CR |= FLASH_CR_PG;
        uint16_t const* src = reinterpret_cast<uint16_t const* const>(t);
        uint16_t* dst = reinterpret_cast<uint16_t*>(addr);
        for (size_t i = 0; i < (sizeof(T) + 1) / 2; i++) {
            *dst = *src;
            ++dst;
            ++src;
            while (FLASH->SR & FLASH_SR_BSY)
                ;
            while (!(FLASH->SR & FLASH_SR_EOP))
                ;
            FLASH->SR &= ~FLASH_SR_EOP;
        }
        return Ok;
    }

    auto erase(size_t addr) const -> Status
    {
        auto region = addrToRegion(addr, regions);
        if (!region) {
            return Invalid_Address;
        }
        FLASH->CR &= ~(FLASH_CR_MER | FLASH_CR_PG);
        FLASH->CR |= FLASH_CR_PER;
        FLASH->AR = addr;
        FLASH->CR |= FLASH_CR_STRT;
        while (FLASH->SR & FLASH_SR_BSY)
            ;
        while (!(FLASH->SR & FLASH_SR_EOP))
            ;
        FLASH->SR &= ~FLASH_SR_EOP;
        return Ok;
    }

    static void unlock(void)
    {
        FLASH->KEYR = 0x45670123;
        FLASH->KEYR = 0xcdef89ab;
    }

    static void lock(void) { FLASH->CR |= FLASH_CR_LOCK; }
};

} // namespace remix::mcu
