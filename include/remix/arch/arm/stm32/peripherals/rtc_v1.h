#pragma once

#include <cstdint>

namespace remix::mcu {

struct Rtc {
    const uint32_t rtcClock;

    auto init() const -> void { RTC->PRER = (63 << 16) | ((rtcClock / 64) - 1); }

    auto enter_init_mode() const -> void
    {
        RTC->ISR |= RTC_ISR_INIT;
        while (!(RTC->ISR & RTC_ISR_INITF))
            ;
    }

    auto exit_init_mode() const -> void { RTC->ISR &= ~RTC_ISR_INIT; }

    auto unlock() const -> void
    {
        RTC->WPR = 0xca;
        RTC->WPR = 0x53;
    }

    auto lock() const -> void { RTC->WPR = 0xff; }

    auto waitForSync() const -> void
    {
        if ((RTC->CR & RTC_CR_BYPSHAD) == 0) {
            RTC->ISR &= ~RTC_ISR_RSF;
            while (!(RTC->ISR & RTC_ISR_RSF))
                ;
        }
    }

    auto setTimeDate(char* date_time) const -> void
    {
        for (char* d = date_time; *d; d++)
            *d -= '0';
        RTC->DR = date_time[2] << 20 | date_time[3] << 16 | date_time[4] << 12 | date_time[5] << 8
            | date_time[6] << 4 | date_time[7] | date_time[8] << 13;
        RTC->TR = date_time[9] << 20 | date_time[10] << 16 | date_time[11] << 12
            | date_time[12] << 8 | date_time[13] << 4 | date_time[14];
    }

    auto getTimeDate() const -> char*
    {
        static char buffer[15];
        uint32_t dr;
        uint32_t tr;

        waitForSync();
        dr = RTC->DR;
        tr = RTC->TR;

        buffer[0] = 2;
        buffer[1] = 0;
        buffer[2] = (dr >> 20) & 0xf;
        buffer[3] = (dr >> 16) & 0xf;
        buffer[4] = (dr >> 12) & 0x1;
        buffer[5] = (dr >> 8) & 0xf;
        buffer[6] = (dr >> 4) & 0x3;
        buffer[7] = dr & 0xf;

        buffer[8] = (tr >> 20) & 0x3;
        buffer[9] = (tr >> 16) & 0xf;
        buffer[10] = (tr >> 12) & 0x7;
        buffer[11] = (tr >> 8) & 0xf;
        buffer[12] = (tr >> 4) & 0x7;
        buffer[13] = tr & 0xf;
        buffer[14] = 0;

        for (size_t i = 0; i < sizeof(buffer) - 1; i++)
            buffer[i] += '0';
        return buffer;
    }
};

}
