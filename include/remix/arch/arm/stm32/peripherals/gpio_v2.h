#pragma once

#include <cstdint>

#include <remix/hal/pin.h>
#include <remix/memptr.h>

namespace remix::mcu {

class Pin {
public:
    enum Mode : uint8_t {
        Input = 0b00,
        Output = 0b01,
        Alternate = 0b10,
        Analog = 0b11,
    };

    enum Pull : uint8_t {
        None = 0b00,
        Up = 0b01,
        Down = 0b10,
    };

    enum OutputType : uint8_t {
        PushPull = 0b0,
        OpenDrain = 0b1,
    };

    enum OutputSpeed {
        Low = 0b00,
        Medium = 0b01,
        High = 0b11,
    };

    uint8_t port;
    uint8_t pin;
    Mode mode { Input };
    Pull pull { None };
    OutputSpeed speed { Low };
    OutputType type { PushPull };
    uint8_t function { 0 };
    bool invert { false };

    auto init() const -> void
    {
        switch (mode) {
        case Input:
            setMode(Input);
            setPullUpDown(pull);
            setType(type);
            break;
        case Output:
            setMode(Output);
            setPullUpDown(pull);
            setSpeed(speed);
            setType(type);
            break;
        case Alternate:
            setMode(Alternate);
            setPullUpDown(pull);
            setSpeed(speed);
            setAlternate(function);
            setType(type);
            break;
        case Analog:
        default:
            setMode(Analog);
            break;
        }
    }

    auto set() const -> void { reg()->BSRR = 1 << (pin + 16 * invert); }
    auto clear() const -> void { reg()->BSRR = 1 << (pin + 16 * (1 - invert)); }
    auto toggle() const -> void
    {
        if (isSet()) {
            clear();
        } else {
            set();
        }
    }
    auto isSet() const -> bool { return invert ^ ((reg()->IDR & (1 << pin)) != 0); }

    auto setMode(Mode m) const -> void
    {
        reg()->MODER = (reg()->MODER & ~(0b11 << (pin * 2))) | (m << (pin * 2));
    }

    auto setPullUpDown(Pull p) const -> void
    {
        reg()->PUPDR = (reg()->PUPDR & ~(0b11 << (pin * 2))) | (p << pin * 2);
    }

    auto setSpeed(OutputSpeed s) const -> void
    {
        reg()->OSPEEDR = (reg()->OSPEEDR & ~(0b11 << (pin * 2))) | (s << pin * 2);
    }

    auto setType(OutputType t) const -> void
    {
        reg()->OTYPER = (reg()->OTYPER & ~(1 << pin)) | (t << pin);
    }

    auto setAlternate(uint8_t f) const -> void
    {
        if (pin < 8) {
            reg()->AFR[0] = (reg()->AFR[0] & ~(0b1111 << (pin * 4))) | (f << (pin * 4));
        } else {
            reg()->AFR[1] = (reg()->AFR[1] & ~(0b1111 << ((pin - 8) * 4))) | (f << ((pin - 8) * 4));
        }
    }

protected:
    auto reg() const -> MemPtr<GPIO_TypeDef, uint32_t>
    {
        return MemPtr<GPIO_TypeDef, uint32_t>(GPIOA_BASE + port * 0x400);
    }
};

}; // namespace remix::mcu
