#pragma once

#include <cstdint>
#include <variant>

#include <remix/hal/pin.h>
#include <remix/memptr.h>

namespace remix::mcu {

class Pin {
public:
    enum Mode { Input, Output, Alternate, Analog };
    enum PinConfig : uint8_t { PushPull = 0b0000, OpenDrain = 0b0100 };
    enum Pull : uint8_t { None = 0b0000, PullUpDown = 0b1000 };
    enum PullDirection : uint8_t { Down = 0, Up = 1 };
    enum Speed : uint8_t { Medium = 0b0001, Low = 0b0010, High = 0b0011 };

    uint8_t port;
    uint8_t pin;
    Mode mode { Input };
    PinConfig pinConfig { PushPull };
    Pull pull { None };
    PullDirection pullDirection { Down };
    Speed speed { Medium };
    uint8_t function { 0 };

    auto init() const -> void
    {
        uint8_t mode_conf = 0;
        switch (mode) {
        case Input:
            mode_conf = pull;
            break;
        case Output:
            mode_conf = static_cast<uint8_t>(pinConfig) | static_cast<uint8_t>(speed);
            break;
        case Alternate:
            mode_conf = static_cast<uint8_t>(pinConfig) | static_cast<uint8_t>(speed)
                | static_cast<uint8_t>(0b1000);
            break;
        case Analog:
            break;
        }

        if (pin < 8) {
            reg()->CRL = (reg()->CRL & ~(0b1111 << (pin * 4))) | (mode_conf << (pin * 4));
        } else {
            reg()->CRH
                = (reg()->CRH & ~(0b1111 << ((pin - 8) * 4))) | (mode_conf << ((pin - 8) * 4));
        }
    }

    auto set() const -> void { reg()->BSRR = (1 << pin); }
    auto clear() const -> void { reg()->BSRR = (1 << (pin + 16)); }
    auto toggle() const -> void {}
    auto isSet() const -> bool { return reg()->IDR & (1 << pin); }

protected:
    auto reg() const -> MemPtr<GPIO_TypeDef, uint32_t>
    {
        return MemPtr<GPIO_TypeDef, uint32_t>(GPIOA_BASE + port * 0x400);
    }
};
}; // namespace remix::mcu
