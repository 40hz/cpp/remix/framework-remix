#pragma once

#include <remix/timeouts.h>
#include <span>
#include <stdint.h>
#include <string_view>

namespace remix::mcu {

struct I2c {
    const uint8_t instance { 0 };
    const uint32_t speed { 100'000 };
    const uint32_t clockSpeed { 4'000'000 };
    enum Direction { Read, Write };

    auto init() const -> void
    {
        reg()->CR1 &= ~I2C_CR1_PE;
        reg()->TIMINGR = (1 << I2C_TIMINGR_PRESC_Pos) | (3 << I2C_TIMINGR_SCLH_Pos)
            | (4 << I2C_TIMINGR_SCLL_Pos);
        reg()->CR1 = I2C_CR1_PE;
    }

    template<typename T = timeouts::Never>
    auto checkTarget(uint8_t address, T timeout = T {}) const -> bool
    {
        reg()->ICR |= I2C_ICR_NACKCF;
        reg()->CR2 = I2C_CR2_AUTOEND | toTargetAddr(address) | I2C_CR2_START;
        while (!timeout.timedOut()) {
            auto isr = reg()->ISR;
            if ((isr & I2C_ISR_STOPF) != 0) {
                return true;
            }
            if ((isr & I2C_ISR_NACKF) != 0) {
                reg()->ICR |= I2C_ICR_NACKCF;
                return false;
            }
        }
        return false;
    }

    template<typename T = timeouts::Never>
    auto controllerSend(uint8_t address, std::basic_string_view<uint8_t> data, bool restart = false,
        T timeout = T {}) const -> bool
    {
        reg()->CR2 = (data.size() << I2C_CR2_NBYTES_Pos) | toTargetAddr(address)
            | (restart ? 0 : I2C_CR2_AUTOEND);
        reg()->CR2 |= I2C_CR2_START;
        if (!waitForInterrupt(I2C_ISR_TXIS, timeout)) {
            return false;
        }
        for (const auto d : data) {
            reg()->TXDR = d;
            if (!waitForInterrupt(I2C_ISR_TC, timeout)) {
                return false;
            }
        }
        reg()->CR2 = I2C_CR2_STOP;
        return true;
    }

    template<typename T = timeouts::Never>
    auto controllerReceive(uint8_t address, std::span<uint8_t> data, T timeout = T {}) const -> bool
    {
        reg()->CR2 = (data.size() << I2C_CR2_NBYTES_Pos) | toTargetAddr(address) | I2C_CR2_RD_WRN
            | I2C_CR2_START;
        for (auto& d : data) {
            if (!waitForInterrupt(I2C_ISR_RXNE, timeout)) {
                return false;
            }
            d = reg()->RXDR & 0xff;
        }
        return true;
    }

    template<typename T = timeouts::Never>
    auto writeRegister(uint8_t address, uint8_t r, std::basic_string_view<uint8_t> data,
        T timeout = {}) const -> bool
    {
        reg()->CR2 = ((1 + data.size()) << I2C_CR2_NBYTES_Pos) | toTargetAddr(address);
        reg()->CR2 |= I2C_CR2_START;
        if (!waitForInterrupt(I2C_ISR_TXIS, timeout)) {
            return false;
        }
        reg()->TXDR = r;
        if (!waitForInterrupt(I2C_ISR_TXIS, timeout)) {
            return false;
        }
        for (const auto d : data) {
            reg()->TXDR = d;
            if (!waitForInterrupt(I2C_ISR_TC, timeout)) {
                return false;
            }
        }
        reg()->CR2 = I2C_CR2_STOP;
        return true;
    }

    template<typename T = timeouts::Never>
    auto readRegister(uint8_t address, uint8_t r, std::span<uint8_t> data, T timeout = {}) const
        -> bool
    {
        reg()->CR2 = (1 << I2C_CR2_NBYTES_Pos) | toTargetAddr(address) | I2C_CR2_START;
        if (!waitForInterrupt(I2C_ISR_TXIS, timeout)) {
            return false;
        }
        reg()->TXDR = r;
        if (!waitForInterrupt(I2C_ISR_TC, timeout)) {
            return false;
        }
        reg()->CR2 = (data.size() << I2C_CR2_NBYTES_Pos) | toTargetAddr(address) | I2C_CR2_RD_WRN
            | I2C_CR2_START;
        for (auto& d : data) {
            if (!waitForInterrupt(I2C_ISR_RXNE, timeout)) {
                return false;
            }
            d = reg()->RXDR & 0xff;
        }
        return true;
    }

    template<typename T = timeouts::Never>
    auto waitForInterrupt(uint32_t interrupt, T timeout = T {}) const -> bool
    {
        while (!timeout.timedOut()) {
            if ((reg()->ISR & interrupt) != 0) {
                return true;
            }
        }
        return false;
    }

    inline auto toTargetAddr(uint8_t address) const -> uint32_t
    {
        return static_cast<uint32_t>(address) << 1;
    }

    inline auto reg() const -> I2C_TypeDef*
    {
        static I2C_TypeDef* const periph[] = {
#ifdef I2C1_BASE
            I2C1,
#else
            0,
#endif
#ifdef I2C2_BASE
            I2C2,
#else
            0,
#endif
        };
        return periph[instance];
    }
};

}
struct I2c {
    auto init() -> void {}
};
