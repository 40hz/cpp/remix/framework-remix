#pragma once

#include <cstdint>

#include <remix/hal/usart.h>
#include <remix/memptr.h>
#include <remix/timeouts.h>

namespace remix::mcu {

struct Usart {
    uint8_t instance;
    uint32_t speed { 115'200 };
    uint32_t clockSpeed { 8'000'000 };

    auto init() const -> void
    {
        reg()->CR1 = USART_CR1_UE | USART_CR1_TE | USART_CR1_RE;
        reg()->BRR = clockSpeed / speed;
    }

    template<typename T = timeouts::Never>
    auto send(uint8_t data, T timeout = T {}) const -> void
    {
        while (!timeout.timedOut() && !canSend())
            ;
        tx(data);
        while (!timeout.timedOut() && !sendComplete())
            ;
    }

    template<typename T = timeouts::Never>
    auto receive(T timeout = T {}) const -> uint8_t
    {
        while (!timeout.timedOut() && !canReceive())
            ;
        return rx();
    }

    auto tx(uint8_t data) const -> void { reg()->TDR = data; }
    auto rx() const -> uint8_t { return static_cast<uint8_t>(reg()->RDR & 0xff); }

    auto canSend() const -> bool { return (reg()->ISR & USART_ISR_TXE) != 0; }
    auto canReceive() const -> bool { return (reg()->ISR & USART_ISR_RXNE) != 0; }
    auto sendComplete() const -> bool { return (reg()->ISR & USART_ISR_TC) != 0; }

    auto enableTxIrq() const -> void { reg()->CR1 |= USART_CR1_TXEIE; }
    auto enableRxIrq() const -> void { reg()->CR1 |= USART_CR1_RXNEIE; }
    auto disableTxIrq() const -> void { reg()->CR1 &= ~USART_CR1_TXEIE; }
    auto disableRxIrq() const -> void { reg()->CR1 &= ~USART_CR1_RXNEIE; }

    template<hal::hasBeginEnd S>
    auto write(const S s) const -> void
    {
        for (auto c : s) {
            send(c);
        }
    }

    static constexpr uint32_t base_addr[] = {
#ifdef USART1_BASE
        USART1_BASE,
#else
        0,
#endif
#ifdef USART2_BASE
        USART2_BASE,
#endif
    };

    auto reg() const -> MemPtr<USART_TypeDef, uint32_t>
    {
        return MemPtr<USART_TypeDef, uint32_t>(base_addr[instance]);
    }

    static constexpr size_t instanceCount = sizeof(base_addr) / sizeof(base_addr[0]);
};

}; // namespace remix::mcu
