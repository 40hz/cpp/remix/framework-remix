#pragma once

#include <remix/flash_region.h>

namespace remix::mcu {

template<auto regions>
class Flash {
public:
    enum Status {
        Ok,
        Invalid_Address,
        Flash_Locked,
        Access_Violation,
    };

    auto init() const -> void {}

    template<typename T>
    auto write(size_t addr, T* t) const -> Status
    {
        auto region = addrToRegion(addr, regions);
        if (!region) {
            return Invalid_Address;
        }
        uint32_t const* src = reinterpret_cast<uint32_t const* const>(t);
        uint32_t* dst = reinterpret_cast<uint32_t*>(addr);
        for (size_t i = 0; i < (sizeof(T) + 1) / 4; i++) {
            *dst = *src;
            ++dst;
            ++src;
            while (FLASH->SR & FLASH_SR_BSY)
                ;
            while (!(FLASH->SR & FLASH_SR_EOP))
                ;
            FLASH->SR &= ~FLASH_SR_EOP;
        }
        return Ok;
    }

    auto erase(size_t addr) const -> Status
    {
        auto region = addrToRegion(addr, regions);
        if (!region) {
            return Invalid_Address;
        }
        FLASH->PECR |= FLASH_PECR_ERASE | FLASH_PECR_PROG;
        *(__IO uint32_t*)addr = 0;
        while (FLASH->SR & FLASH_SR_BSY)
            ;
        while (!(FLASH->SR & FLASH_SR_EOP))
            ;
        FLASH->SR &= ~FLASH_SR_EOP;
        FLASH->PECR &= ~(FLASH_PECR_ERASE | FLASH_PECR_PROG);
        return Ok;
    }

    static void unlock(void)
    {
        FLASH->PEKEYR = 0x45670123;
        FLASH->PEKEYR = 0xcdef89ab;
    }

    static void lock(void) { FLASH->PECR |= FLASH_PECR_PELOCK; }
};

} // namespace remix::mcu
