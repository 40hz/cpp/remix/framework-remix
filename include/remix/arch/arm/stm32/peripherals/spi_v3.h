#pragma once

#include <cstdint>
#include <variant>

#include <remix/hal/usart.h>
#include <remix/memptr.h>

namespace remix::mcu {

class Spi {
public:
    enum ClockPolarity : uint8_t { IdleLow = 0b0, IdleHigh = 0b1 };
    enum ClockPhase : uint8_t { CaptureOnFirst = 0b0, CaptureOnSecond = 0b1 };

    uint8_t instance;
    uint32_t speed { 1'000'000 };
    ClockPolarity clockPolarity { IdleLow };
    ClockPhase clockPhase { CaptureOnFirst };
    bool isMaster { true };
    bool isLsbFirst { false };

    auto init() const -> void
    {
        reg()->CR1 = 0;
        reg()->CR1 = reg()->CR1 | (isMaster ? SPI_CR1_MSTR : 0)
            | (isLsbFirst ? SPI_CR1_LSBFIRST : 0) | (clockPolarity << SPI_CR1_CPOL_Pos)
            | (clockPhase << SPI_CR1_CPHA_Pos) | SPI_CR1_SSI | SPI_CR1_SSM;
        reg()->CR1 = reg()->CR1 | SPI_CR1_SPE;
    }

    auto transmit(uint8_t data) const -> uint8_t
    {
        while (!canSend())
            ;
        *(reinterpret_cast<volatile uint8_t*>(&reg()->DR)) = data;
        while (!canReceive() || isBusy())
            ;
        return *(reinterpret_cast<volatile uint8_t*>(&reg()->DR));
    }
    auto send(uint8_t data) const -> void { transmit(data); }
    auto receive() const -> uint8_t { return transmit(0); }
    auto canSend() const -> bool { return (reg()->SR & SPI_SR_TXE) != 0; }
    auto canReceive() const -> bool { return (reg()->SR & SPI_SR_RXNE) != 0; }
    auto isBusy() const -> bool { return (reg()->SR & SPI_SR_BSY) != 0; }

    template<hal::hasBeginEnd S>
    auto write(const S s) const -> void
    {
        for (auto c : s) {
            while (!canSend())
                ;
            send(c);
        }
    }

    template<hal::hasBeginEnd S>
    auto read(S s) const -> void
    {
        for (auto& c : s) {
            c = receive();
        }
    }

protected:
    auto reg() const -> MemPtr<SPI_TypeDef, uint32_t>
    {
        static constexpr uint32_t base_addr[] = {
            SPI1_BASE,
#ifdef SPI2_BASE
            SPI2_BASE,
#endif
#ifdef SPI3_BASE
            SPI3_BASE,
#endif
#ifdef SPI4_BASE
            SPI4_BASE,
#endif
        };
        return MemPtr<SPI_TypeDef, uint32_t>(base_addr[instance]);
    }
};

}; // namespace remix::mcu
