#pragma once

namespace remix {

template<typename T, typename B>
class MemPtr {
    B addr;

public:
    constexpr MemPtr(B i)
        : addr { i }
    {
    }

    operator T*() const { return reinterpret_cast<T*>(addr); }
    T* operator->() const { return operator T*(); }
};

} // namespace remix
