#pragma once

#include <array>
#include <cstddef>
#include <expected>

#include <remix/timeouts.h>
#include <span>

namespace drivers::nrf24 {

enum Register : uint8_t {
    CONFIG = 0x00,
    EN_AA = 0x01,
    EN_RXADDR = 0x02,
    SETUP_AW = 0x03,
    SETUP_RETR = 0x04,
    RF_CH = 0x05,
    RF_SETUP = 0x06,
    STATUS = 0x07,
    OBSERVE_TX = 0x08,
    RPD = 0x09,
    RX_ADDR_P0 = 0x0A,
    RX_ADDR_P1 = 0x0B,
    RX_ADDR_P2 = 0x0C,
    RX_ADDR_P3 = 0x0D,
    RX_ADDR_P4 = 0x0E,
    RX_ADDR_P5 = 0x0F,
    TX_ADDR = 0x10,
    RX_PW_P0 = 0x11,
    RX_PW_P1 = 0x12,
    RX_PW_P2 = 0x13,
    RX_PW_P3 = 0x14,
    RX_PW_P4 = 0x15,
    RX_PW_P5 = 0x16,
    FIFO_STATUS = 0x17,
    DYNPD = 0x1C,
    FEATURE = 0x1D,
};

enum Command : uint8_t {
    R_REGISTER = 0b0000'0000,
    W_REGISTER = 0b0010'0000,
    R_RX_PL_WID = 0b0110'0000,
    R_RX_PAYLOAD = 0b0110'0001,
    W_TX_PAYLOAD = 0b1010'0000,
    W_ACK_PAYLOAD = 0b1010'1000,
    W_TX_PAYLOAD_NOACK = 0b1011'0000,
    FLUSH_TX = 0b1110'0001,
    FLUSH_RX = 0b1110'0010,
    REUSE_TX_PL = 0b1110'0011,
    NOP = 0b1111'1111,
};

static constexpr auto bit(uint8_t n) -> uint8_t { return (1 << n); }

static const uint8_t CONFIG_EN_CRC = bit(3);
static const uint8_t CONFIG_CRCO = bit(2);
static const uint8_t CONFIG_PWR_UP = bit(1);
static const uint8_t CONFIG_PRIM_RX = bit(0);
static const uint8_t EN_AA_ENAA_P5 = bit(5);
static const uint8_t EN_AA_ENAA_P4 = bit(4);
static const uint8_t EN_AA_ENAA_P3 = bit(3);
static const uint8_t EN_AA_ENAA_P2 = bit(2);
static const uint8_t EN_AA_ENAA_P1 = bit(1);
static const uint8_t EN_AA_ENAA_P0 = bit(0);
static const uint8_t EN_RXADDR_ERX_P5 = bit(5);
static const uint8_t EN_RXADDR_ERX_P4 = bit(4);
static const uint8_t EN_RXADDR_ERX_P3 = bit(3);
static const uint8_t EN_RXADDR_ERX_P2 = bit(2);
static const uint8_t EN_RXADDR_ERX_P1 = bit(1);
static const uint8_t EN_RXADDR_ERX_P0 = bit(0);
static const uint8_t DYNPD_DPL_P5 = bit(5);
static const uint8_t DYNPD_DPL_P4 = bit(4);
static const uint8_t DYNPD_DPL_P3 = bit(3);
static const uint8_t DYNPD_DPL_P2 = bit(2);
static const uint8_t DYNPD_DPL_P1 = bit(1);
static const uint8_t DYNPD_DPL_P0 = bit(0);
static const uint8_t SETUP_AW_AW_3BYTES = 0;
static const uint8_t SETUP_AW_AW_4BYTES = bit(0);
static const uint8_t SETUP_AW_AW_5BYTES = bit(0) + bit(1);
static const uint8_t RF_SETUP_PLL_LOCK = bit(4);
static const uint8_t RF_SETUP_CONT_WAVE = bit(7);
static const uint8_t RF_SETUP_RF_DR = bit(3);
static const uint8_t RF_SETUP_RF_DR_LOW = bit(5);
static const uint8_t RF_SETUP_RF_DR_HIGH = bit(3);
static const uint8_t RF_SETUP_RF_NEG18DBM = 0;
static const uint8_t RF_SETUP_RF_NEG12DBM = bit(1);
static const uint8_t RF_SETUP_RF_NEG6DBM = bit(2);
static const uint8_t RF_SETUP_RF_0DBM = bit(1) + bit(2);
static const uint8_t STATUS_RX_DR = bit(6);
static const uint8_t STATUS_TX_DS = bit(5);
static const uint8_t STATUS_MAX_RT = bit(4);
static const uint8_t STATUS_RX_P_NO = bit(1);
static const uint8_t STATUS_TX_FULL = bit(0);
static const uint8_t FIFO_STATUS_TX_REUSE = bit(6);
static const uint8_t FIFO_STATUS_FIFO_FULL = bit(5);
static const uint8_t FIFO_STATUS_TX_EMPTY = bit(4);
static const uint8_t FIFO_STATUS_RX_FULL = bit(1);
static const uint8_t FIFO_STATUS_RX_EMPTY = bit(0);
static const uint8_t FEATURE_EN_DPL = bit(2);
static const uint8_t FEATURE_EN_ACK_PAY = bit(1);
static const uint8_t FEATURE_EN_DYN_ACK = bit(0);

struct RegisterValue {
    Register reg;
    uint8_t value;
};

static constexpr std::array<RegisterValue, 8> initValues { {
    { CONFIG, CONFIG_EN_CRC },
    { STATUS, STATUS_RX_DR | STATUS_TX_DS | STATUS_MAX_RT },
    { SETUP_AW, SETUP_AW_AW_5BYTES },
    { EN_RXADDR, EN_RXADDR_ERX_P0 | EN_RXADDR_ERX_P1 },
    { RF_SETUP, RF_SETUP_RF_DR_LOW | RF_SETUP_RF_0DBM },
    { FEATURE, FEATURE_EN_DPL | FEATURE_EN_DYN_ACK },
    { DYNPD, DYNPD_DPL_P0 | DYNPD_DPL_P1 },
    { EN_AA, 0 },
} };

static constexpr std::array printableRegisters { CONFIG, EN_AA, EN_RXADDR, SETUP_AW, SETUP_RETR,
    RF_CH, RF_SETUP, STATUS, OBSERVE_TX, RPD, RX_PW_P0, RX_PW_P1, RX_PW_P2, RX_PW_P3, RX_PW_P4,
    RX_PW_P5, FIFO_STATUS, DYNPD, FEATURE };

enum class Error {
    Timeout,
};

template<typename Spi, typename Pin>
class Driver {
public:
    auto init() const -> void
    {
        for (auto i : initValues) {
            writeRegister(i.reg, i.value);
        }
    }

    auto readRegister(Register r) const -> uint8_t
    {
        csn.clear();
        spi.send(r | static_cast<uint8_t>(R_REGISTER));
        uint8_t v = spi.receive();
        csn.set();
        return v;
    }

    auto writeRegister(Register r, uint8_t v) const -> void
    {
        csn.clear();
        spi.send(r | static_cast<uint8_t>(W_REGISTER));
        spi.send(v);
        csn.set();
    }

    auto setChannel(uint8_t channel) const -> void { writeRegister(RF_CH, channel); }

    auto getChannel() const -> uint8_t { return readRegister(RF_CH); }

    auto setAddress(Register r, std::span<uint8_t const> address) const -> void
    {
        csn.clear();
        spi.send(r | static_cast<uint8_t>(W_REGISTER));
        for (auto a : address) {
            spi.send(a);
        }
        csn.set();
    }

    auto setRxAddress(std::span<uint8_t const> address) const -> void
    {
        setAddress(RX_ADDR_P1, address);
    }

    auto setTxAddress(std::span<uint8_t const> address) const -> void
    {
        setAddress(TX_ADDR, address);
        setAddress(RX_ADDR_P0, address);
    }

    auto txMode() const -> void
    {
        writeRegister(CONFIG, CONFIG_EN_CRC | CONFIG_PWR_UP);
        ce.clear();
    }

    auto rxMode() const -> void
    {
        writeRegister(CONFIG, CONFIG_EN_CRC | CONFIG_PRIM_RX | CONFIG_PWR_UP);
        ce.set();
    }

    auto idle() const -> void { ce.clear(); }

    auto powerDown() const -> void { ce.clear(); }

    auto rxAvailable() const -> bool { return (readRegister(STATUS) & STATUS_RX_DR) != 0; }

    template<class Timeout>
    auto tx(auto data, Timeout timeout) const -> std::expected<int, Error>
    {
        txMode();
        csn.clear();
        spi.send(FLUSH_TX);
        csn.set();
        csn.clear();
        spi.send(W_TX_PAYLOAD_NOACK);
        for (auto d : data) {
            spi.send(d);
        }
        csn.set();
        ce.set();
        while (true) {
            uint8_t status = readRegister(STATUS);
            if ((status & STATUS_TX_DS) != 0) {
                writeRegister(STATUS, STATUS_TX_DS);
                ce.clear();
                return 0;
            }
            if (timeout.timedOut()) {
                ce.clear();
                return std::unexpected(Error::Timeout);
            }
        }
    }

    auto readPayload(auto& data) const -> size_t
    {
        csn.clear();
        spi.send(R_RX_PL_WID);
        const auto n = std::min(static_cast<size_t>(spi.receive()), data.size());
        csn.set();
        csn.clear();
        spi.send(R_RX_PAYLOAD);
        for (auto& d : data) {
            d = spi.receive();
        }
        csn.set();
        csn.clear();
        spi.send(FLUSH_RX);
        csn.set();
        return n;
    }

    template<class Timeout>
    auto rx(auto& data, Timeout timeout) const -> std::expected<size_t, Error>
    {
        rxMode();
        while (true) {
            if (timeout.timedOut()) {
                return std::unexpected(Error::Timeout);
            }
            if (rxAvailable())
                break;
        }
        writeRegister(STATUS, STATUS_RX_DR);
        return readPayload(data);
    }

    auto getRegisters() const -> std::array<RegisterValue, printableRegisters.size()>
    {
        std::array<RegisterValue, printableRegisters.size()> values;
        size_t n = 0;
        for (auto r : printableRegisters) {
            values[n] = { r, readRegister(r) };
            n++;
        }
        return values;
    }

    const Spi& spi;
    const Pin& csn;
    const Pin& ce;
};

} // namespace drivers::nrf24
