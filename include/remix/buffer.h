#pragma once

#include <span>

namespace remix {

template<typename T>
struct Buffer {
    std::span<T>::iterator current {};
    std::span<T>::iterator end {};

    auto init(std::span<T> buffer) -> void
    {
        current = buffer.begin();
        end = buffer.end();
    }

    auto atEnd() -> bool { return current >= end; }
};

}
