#pragma once

#if defined(REMIX_MCU_FAMILY_STM32F0)
#include <remix/arch/arm/stm32/stm32f0.h>
#elif defined(REMIX_MCU_FAMILY_STM32F1)
#include <remix/arch/arm/stm32/stm32f1.h>
#elif defined(REMIX_MCU_FAMILY_STM32F3)
#include <remix/arch/arm/stm32/stm32f3.h>
#elif defined(REMIX_MCU_FAMILY_STM32L0)
#include <remix/arch/arm/stm32/stm32l0.h>
#endif
