#pragma once

#include <avr/io.h>
#include <remix/arch/avr/atmega328.h>

namespace remix::board {

const mcu::Pin led { .port = mcu::Pin::Port_B, .pin = 5, .direction = mcu::Pin::Output };
const mcu::Pin sck { .port = mcu::Pin::Port_B, .pin = 5, .direction = mcu::Pin::Output };
const mcu::Pin sdi { .port = mcu::Pin::Port_B, .pin = 4, .direction = mcu::Pin::Input };
const mcu::Pin sdo { .port = mcu::Pin::Port_B, .pin = 3, .direction = mcu::Pin::Output };
const mcu::Pin csn { .port = mcu::Pin::Port_B, .pin = 2, .direction = mcu::Pin::Output };
const mcu::Pin tx { .port = mcu::Pin::Port_D, .pin = 1, .direction = mcu::Pin::Output };
const mcu::Pin rx { .port = mcu::Pin::Port_D, .pin = 0, .direction = mcu::Pin::Output };

namespace nrf24 {
const mcu::Pin ce { .port = mcu::Pin::Port_D, .pin = 6, .direction = mcu::Pin::Output };
const mcu::Pin irq { .port = mcu::Pin::Port_D, .pin = 5, .direction = mcu::Pin::Input };
}

const mcu::Usart usart {};
const mcu::Usart serial = usart;
const mcu::Spi spi {};

inline auto init() -> void {}

}; // namespace remix::board
