#pragma once

#include <avr/io.h>
#include <remix/arch/avr/avr_dx.h>

namespace remix::board {

const mcu::Pin led { .port = mcu::Pin::Port_A, .pin = 7, .direction = mcu::Pin::Output };
const mcu::Pin button { .port = mcu::Pin::Port_D, .pin = 7, .direction = mcu::Pin::Input };
const mcu::Pin sck { .port = mcu::Pin::Port_A, .pin = 6, .direction = mcu::Pin::Output };
const mcu::Pin sdi { .port = mcu::Pin::Port_A, .pin = 5, .direction = mcu::Pin::Input };
const mcu::Pin sdo { .port = mcu::Pin::Port_A, .pin = 4, .direction = mcu::Pin::Output };
const mcu::Pin csn { .port = mcu::Pin::Port_C, .pin = 0, .direction = mcu::Pin::Output };
const mcu::Pin tx { .port = mcu::Pin::Port_A, .pin = 0, .direction = mcu::Pin::Output };
const mcu::Pin rx { .port = mcu::Pin::Port_A, .pin = 1, .direction = mcu::Pin::Output };
const mcu::Pin scl {
    .port = mcu::Pin::Port_A, .pin = 3, .direction = mcu::Pin::Output, .pullup = true
};
const mcu::Pin sclOut { .port = mcu::Pin::Port_A, .pin = 3, .direction = mcu::Pin::Output };
const mcu::Pin sda {
    .port = mcu::Pin::Port_A, .pin = 2, .direction = mcu::Pin::Input, .pullup = true
};

namespace nrf24 {
const mcu::Pin ce { .port = mcu::Pin::Port_C, .pin = 1, .direction = mcu::Pin::Output };
const mcu::Pin irq { .port = mcu::Pin::Port_C, .pin = 2, .direction = mcu::Pin::Input };
}

const mcu::Usart usart {};
const mcu::Usart serial = usart;
const mcu::Spi spi {};
const mcu::I2c i2c {};

inline auto init() -> void {}

}; // namespace remix::board
