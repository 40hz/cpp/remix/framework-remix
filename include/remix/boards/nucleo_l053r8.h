#pragma once

#include <array>
#include <cstddef>

#include <remix/arch/arm/stm32/stm32l0.h>
#include <remix/flash_region.h>

namespace remix::board {

extern "C" std::uint8_t __rom_end__;

const mcu::Pin led { .port = 0, .pin = 8, .mode = mcu::Pin::Output };
const mcu::Pin button { .port = 2, .pin = 13, .mode = mcu::Pin::Input, .invert = true };
const mcu::Pin csn { .port = 1, .pin = 6, .mode = mcu::Pin::Output };
const mcu::Pin sck {
    .port = 0, .pin = 5, .mode = mcu::Pin::Alternate, .speed = mcu::Pin::High, .function = 0
};
const mcu::Pin sdi {
    .port = 0, .pin = 6, .mode = mcu::Pin::Alternate, .speed = mcu::Pin::High, .function = 0
};
const mcu::Pin sdo {
    .port = 0, .pin = 7, .mode = mcu::Pin::Alternate, .speed = mcu::Pin::High, .function = 0
};
const mcu::Pin tx { .port = 0, .pin = 2, .mode = mcu::Pin::Alternate, .function = 4 };
const mcu::Pin rx { .port = 0, .pin = 3, .mode = mcu::Pin::Alternate, .function = 4 };
const mcu::Pin sda {
    .port = 1, .pin = 8, .mode = mcu::Pin::Alternate, .type = mcu::Pin::OpenDrain, .function = 4
};
const mcu::Pin scl {
    .port = 1, .pin = 9, .mode = mcu::Pin::Alternate, .type = mcu::Pin::OpenDrain, .function = 4
};

const mcu::Usart usart { .instance = 1, .clockSpeed = 2'100'000 };
const mcu::Spi spi { .instance = 0, .isMaster = true };
const mcu::I2c i2c { .instance = 0, .speed = 400'000, .clockSpeed = 2'100'000 };

const mcu::Usart serial = usart;

const size_t endOfRom = reinterpret_cast<size_t>(&__rom_end__);
const size_t infoSegment = endOfRom - 128;

constexpr std::array<FlashRegion, 2> flashRegions { {
    { 0x0800'0000, 65536, 128, 128, 4 },
} };

constexpr mcu::Flash<flashRegions> flash;

inline auto wakeup() -> void
{
    RCC->IOPENR = RCC_IOPENR_GPIOAEN | RCC_IOPENR_GPIOBEN | RCC_IOPENR_GPIOCEN;
    RCC->APB1ENR = RCC_APB1ENR_USART2EN | RCC_APB1ENR_I2C1EN | RCC_APB1ENR_PWREN;
    RCC->APB2ENR = RCC_APB2ENR_SPI1EN | RCC_APB2ENR_SYSCFGEN;
}

inline auto sleep() -> void
{
    uint32_t gpioAMode = GPIOA->MODER;
    uint32_t gpioAPupd = GPIOA->PUPDR;
    uint32_t gpioBMode = GPIOB->MODER;
    uint32_t gpioBPupd = GPIOB->PUPDR;

    GPIOA->MODER = 0xffff'ffff;
    GPIOA->PUPDR = 0x0000'0000;
    GPIOB->MODER = 0xffff'ffff;
    GPIOB->PUPDR = 0x0000'0000;

    RCC->IOPENR &= ~(RCC_IOPENR_GPIOBEN | RCC_IOPENR_GPIOCEN);
    RCC->APB1ENR &= ~(RCC_APB1ENR_USART2EN | RCC_APB1ENR_I2C1EN | RCC_APB1ENR_PWREN);
    RCC->APB2ENR &= ~RCC_APB2ENR_SPI1EN;

    asm("wfi");

    wakeup();

    GPIOA->MODER = gpioAMode;
    GPIOA->PUPDR = gpioAPupd;
    GPIOB->MODER = gpioBMode;
    GPIOB->PUPDR = gpioBPupd;
}

inline auto init() -> void
{
    wakeup();
    PWR->CR |= PWR_CR_DBP;
    RCC->CSR |= RCC_CSR_LSEON;
    while ((RCC->CSR & RCC_CSR_LSERDY) == 0) {
    }

    RCC->CSR |= RCC_CSR_RTCEN | RCC_CSR_RTCSEL_LSE;
}
}; // namespace remix::board
