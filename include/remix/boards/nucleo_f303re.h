#pragma once

#include <array>
#include <cstddef>

#include <remix/arch/arm/stm32/stm32f3.h>
#include <remix/flash_region.h>

namespace remix::board {

extern "C" std::uint8_t __rom_end__;

const uint32_t clockSpeed = 72'000'000;

const mcu::Pin led { .port = 1, .pin = 11, .mode = mcu::Pin::Output };
namespace nrf24 {
const mcu::Pin ce { .port = 2, .pin = 7, .mode = mcu::Pin::Output };
}
const mcu::Pin csn { .port = 1, .pin = 6, .mode = mcu::Pin::Output };
const mcu::Pin sck { .port = 0, .pin = 5, .mode = mcu::Pin::Alternate, .function = 5 };
const mcu::Pin sdi { .port = 0, .pin = 6, .mode = mcu::Pin::Alternate, .function = 5 };
const mcu::Pin sdo { .port = 0, .pin = 7, .mode = mcu::Pin::Alternate, .function = 5 };
const mcu::Pin tx { .port = 0, .pin = 2, .mode = mcu::Pin::Alternate, .function = 7 };
const mcu::Pin rx { .port = 0, .pin = 3, .mode = mcu::Pin::Alternate, .function = 7 };

const mcu::Usart usart { .instance = 1, .clockSpeed = clockSpeed / 2 };
const mcu::Spi spi { .instance = 0, .isMaster = true, .clockSpeed = clockSpeed };

const mcu::Usart serial = usart;

const size_t endOfRom = reinterpret_cast<size_t>(&__rom_end__);
const size_t infoSegment = endOfRom - 2048;

constexpr std::array<FlashRegion, 2> flashRegions { {
    { 0x0800'0000, 512 * 1024, 2048, 2048, 2 },
} };

constexpr mcu::Flash<flashRegions> flash;

inline auto init() -> void
{
    RCC->AHBENR = RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN | RCC_AHBENR_GPIOCEN | RCC_AHBENR_FLITFEN;
    RCC->APB1ENR = RCC_APB1ENR_USART2EN;
    RCC->APB2ENR = RCC_APB2ENR_SPI1EN;

    FLASH->ACR = FLASH_ACR_PRFTBE | FLASH_ACR_LATENCY_2;

    RCC->CR &= ~RCC_CR_PLLON;
    while (RCC->CR & RCC_CR_PLLRDY)
        ;

    RCC->CFGR = RCC_CFGR_PLLMUL9 | RCC_CFGR_USBPRE_DIV1_5 | RCC_CFGR_PLLSRC_HSI_PREDIV
        | RCC_CFGR_PPRE1_DIV2;
    RCC->CFGR2 = RCC_CFGR2_ADCPRE12_DIV12 | RCC_CFGR2_ADCPRE34_DIV12;

    RCC->CR |= RCC_CR_PLLON;
    while (!(RCC->CR & RCC_CR_PLLRDY))
        ;

    RCC->CFGR |= RCC_CFGR_SW_PLL;
    while (!(RCC->CFGR & RCC_CFGR_SWS_PLL))
        ;
}

inline auto sleep() -> void { asm("wfi"); }

}; // namespace remix::board
