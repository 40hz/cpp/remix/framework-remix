#pragma once

#include <array>
#include <cstddef>

#include <remix/arch/arm/stm32/stm32l0.h>
#include <remix/flash_region.h>

namespace remix::board {

extern "C" std::uint8_t __rom_end__;

const mcu::Pin led { .port = 1, .pin = 1, .mode = mcu::Pin::Output };
const mcu::Pin button { .port = 1, .pin = 0, .mode = mcu::Pin::Input };
const mcu::Pin csn { .port = 0, .pin = 11, .mode = mcu::Pin::Output };
const mcu::Pin sck { .port = 1, .pin = 3, .mode = mcu::Pin::Alternate, .function = 0 };
const mcu::Pin sdi { .port = 1, .pin = 4, .mode = mcu::Pin::Alternate, .function = 0 };
const mcu::Pin sdo { .port = 1, .pin = 5, .mode = mcu::Pin::Alternate, .function = 0 };
const mcu::Pin tx { .port = 0, .pin = 2, .mode = mcu::Pin::Alternate, .function = 4 };
const mcu::Pin rx { .port = 0, .pin = 15, .mode = mcu::Pin::Alternate, .function = 4 };
const mcu::Pin sda {
    .port = 1, .pin = 6, .mode = mcu::Pin::Alternate, .type = mcu::Pin::OpenDrain, .function = 1
};
const mcu::Pin scl {
    .port = 1, .pin = 7, .mode = mcu::Pin::Alternate, .type = mcu::Pin::OpenDrain, .function = 1
};

const mcu::Usart usart { .instance = 1, .clockSpeed = 2'100'000 };
const mcu::Spi spi { .instance = 0, .isMaster = true };
const mcu::I2c i2c { .instance = 0, .speed = 400'000, .clockSpeed = 2'100'000 };

const mcu::Usart serial = usart;

const size_t endOfRom = reinterpret_cast<size_t>(&__rom_end__);
const size_t infoSegment = endOfRom - 128;

constexpr std::array<FlashRegion, 2> flashRegions { {
    { 0x0800'0000, 32768, 128, 128, 4 },
} };

constexpr mcu::Flash<flashRegions> flash;

inline auto sleep() -> void { asm("wfi"); }

inline auto init() -> void
{
    RCC->IOPENR = RCC_IOPENR_GPIOAEN | RCC_IOPENR_GPIOBEN | RCC_IOPENR_GPIOCEN;
    RCC->APB1ENR = RCC_APB1ENR_USART2EN | RCC_APB1ENR_PWREN;
    RCC->APB2ENR = RCC_APB2ENR_SPI1EN;

    PWR->CR |= PWR_CR_DBP;
    RCC->CSR |= RCC_CSR_LSEON;
    while ((RCC->CSR & RCC_CSR_LSERDY) == 0) {
    }

    RCC->CSR |= RCC_CSR_RTCEN | RCC_CSR_RTCSEL_LSE;
}
}; // namespace remix::board
