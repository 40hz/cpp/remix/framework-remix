#pragma once

#include <remix/arch/msp430/msp430fr2_4.h>

namespace remix::board {

const mcu::Pin led { .port = 0, .pin = 3, .direction = mcu::Pin::Output };
const mcu::Pin xt1in { .port = 3, .pin = 1, .alt = true };
const mcu::Pin xt1out { .port = 3, .pin = 2, .alt = true };
const mcu::Pin led2 { .port = 3, .pin = 0, .direction = mcu::Pin::Output };
const mcu::Pin button { .port = 0, .pin = 2, .direction = mcu::Pin::Input };
const mcu::Pin rx { .port = 0, .pin = 1, .alt = true };
const mcu::Pin tx { .port = 0, .pin = 0, .alt = true };
const mcu::Pin sck { .port = 4, .pin = 1, .alt = true };
const mcu::Pin sdo { .port = 4, .pin = 2, .alt = true };
const mcu::Pin sdi { .port = 4, .pin = 3, .alt = true };
const mcu::Pin csn { .port = 7, .pin = 2, .direction = mcu::Pin::Output };
const mcu::ClockSystem clocks {
    .mClock = { .type = mcu::ClockSystem::Clock::MClk,
        .source = mcu::ClockSystem::Clock::DcoClk,
        .frequency = 12'000'000 },
    .smClock = { .type = mcu::ClockSystem::Clock::SmClk,
        .source = mcu::ClockSystem::Clock::DcoClk,
        .frequency = 12'000'000 },
};

const mcu::Uart usart { .speed = 115200, .clock = clocks.smClock };
const mcu::Spi spi {
    .eusci_module = decltype(spi)::eUSCI_B, .speed = 4'000'000, .clock = clocks.smClock
};
const mcu::Uart serial = usart;

namespace nrf24 {
const mcu::Pin ce { .port = 1, .pin = 5, .direction = mcu::Pin::Output };
const mcu::Pin irq { .port = 8, .pin = 3, .direction = mcu::Pin::Input };
} // namespace nrf24

auto init() -> void
{
    WDTCTL = WDTPW | WDTHOLD;
    PM5CTL0 &= ~LOCKLPM5;
    xt1in.init();
    xt1out.init();
    clocks.init();
}

extern "C" void _fini() {}

} // namespace remix::board
