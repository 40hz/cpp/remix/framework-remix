#pragma once

#include <array>

#include <remix/arch/msp430/msp430g2.h>

namespace remix::board {

constexpr mcu::Pin led { .port = 0, .pin = 0, .direction = mcu::Pin::Output };
constexpr mcu::Pin button { .port = 0, .pin = 3, .direction = mcu::Pin::Input };
constexpr mcu::Pin rx { .port = 0, .pin = 1, .function = mcu::Pin::Secondary };
constexpr mcu::Pin tx { .port = 0, .pin = 2, .function = mcu::Pin::Secondary };
constexpr mcu::Pin sck { .port = 0, .pin = 5, .function = mcu::Pin::Secondary };
constexpr mcu::Pin sdo { .port = 0, .pin = 7, .function = mcu::Pin::Secondary };
constexpr mcu::Pin sdi { .port = 0, .pin = 6, .function = mcu::Pin::Secondary };
constexpr mcu::Pin csn { .port = 1, .pin = 1, .direction = mcu::Pin::Output };
constexpr mcu::ClockModule clocks {
    .mClock = { .type = mcu::ClockModule::Clock::MClk,
        .source = mcu::ClockModule::Clock::DcoClk,
        .frequency = 8'000'000 },
    .smClock = { .type = mcu::ClockModule::Clock::SmClk,
        .source = mcu::ClockModule::Clock::DcoClk,
        .frequency = 8'000'000 },
};
constexpr mcu::Uart usart { .speed = 57600, .clock = clocks.smClock };
constexpr mcu::Spi spi {
    .usci_module = decltype(spi)::USCI_B, .speed = 4'000'000, .clock = clocks.smClock
};

constexpr mcu::Uart serial = usart;

namespace nrf24 {
constexpr mcu::Pin ce { .port = 1, .pin = 0, .direction = mcu::Pin::Output };
constexpr mcu::Pin irq { .port = 1, .pin = 2, .direction = mcu::Pin::Input };
} // namespace nrf24

constexpr std::array<FlashRegion, 2> flashRegions { {
    { 0x1000, 0x40 * 4, 0x40, 0x40, 2 },
    { 0xc000, 0x200 * 32, 0x40, 0x40, 2 },
} };

constexpr size_t infoSegment = 0x1000;

constexpr mcu::Flash<board::flashRegions, mcu::ClockModule::Clock> flash { .clock
    = clocks.smClock };

auto init() -> void
{
    WDTCTL = WDTPW | WDTHOLD;
    clocks.init();
}

extern "C" void _fini() {}

} // namespace remix::board
