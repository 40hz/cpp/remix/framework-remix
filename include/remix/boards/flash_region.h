#pragma once

#include <cstddef>
#include <expected>

struct FlashRegion {
    size_t start;
    size_t size;
    size_t eraseGranularity;
    size_t writeGranularity;
    size_t writeSize;
};

struct RegionError {};

auto constexpr addrToRegion(size_t addr, auto regions) -> std::expected<FlashRegion, RegionError>
{
    for (auto r : regions) {
        if (addr >= r.start && addr < r.start + r.size) {
            return r;
        }
    }
    return std::unexpected { RegionError {} };
}
