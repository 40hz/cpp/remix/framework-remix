#pragma once

#include <remix/arch/arm/stm32/stm32f0.h>

namespace remix::board {

namespace nrf24 {
const mcu::Pin ce { .port = 2, .pin = 7, .mode = mcu::Pin::Output };
}
const mcu::Pin led { .port = 0, .pin = 0, .mode = mcu::Pin::Mode::Output };
const mcu::Pin button { .port = 1, .pin = 0, .mode = mcu::Pin::Mode::Output };
const mcu::Pin chip_select { .port = 2, .pin = 0, .mode = mcu::Pin::Mode::Output };
const mcu::Pin tx { .port = 0, .pin = 2, .mode = mcu::Pin::Alternate, .function = 1 };
const mcu::Pin rx { .port = 0, .pin = 3, .mode = mcu::Pin::Alternate, .function = 1 };
const mcu::Pin csn { .port = 1, .pin = 6, .mode = mcu::Pin::Output, .speed = mcu::Pin::High };
const mcu::Pin sck {
    .port = 0, .pin = 5, .mode = mcu::Pin::Alternate, .speed = mcu::Pin::High, .function = 0
};
const mcu::Pin sdi {
    .port = 0, .pin = 6, .mode = mcu::Pin::Alternate, .speed = mcu::Pin::High, .function = 0
};
const mcu::Pin sdo {
    .port = 0, .pin = 7, .mode = mcu::Pin::Alternate, .speed = mcu::Pin::High, .function = 0
};
const mcu::Pin sda {
    .port = 1, .pin = 8, .mode = mcu::Pin::Alternate, .type = mcu::Pin::OpenDrain, .function = 4
};
const mcu::Pin scl {
    .port = 1, .pin = 9, .mode = mcu::Pin::Alternate, .type = mcu::Pin::OpenDrain, .function = 4
};

const mcu::Usart usart { .instance = 1 };
const mcu::Spi spi { .instance = 0, .isMaster = true };
const mcu::I2c i2c { .instance = 0, .speed = 400'000, .clockSpeed = 8'000'000 };

const mcu::Usart serial = usart;

inline auto sleep() -> void { asm("wfi"); }

inline auto init() -> void
{
    RCC->AHBENR = RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN | RCC_AHBENR_GPIOCEN | RCC_AHBENR_FLITFEN;
    RCC->APB1ENR = RCC_APB1ENR_USART2EN | RCC_APB1ENR_I2C1EN;
    RCC->APB2ENR = RCC_APB2ENR_SPI1EN;
}

}; // namespace remix::board
