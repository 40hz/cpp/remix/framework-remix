#pragma once

#include <cstdint>

namespace remix::timeouts {

class Never {
public:
    auto timedOut() -> bool { return false; }
};

class Immediately {
public:
    auto timedOut() -> bool { return true; }
};

class Counter {
public:
    Counter(uint32_t t)
        : timeout(t)
    {
    }
    Counter()
        : timeout { 0 }
    {
    }
    auto set(uint32_t t) -> void { timeout = t; }
    auto get() -> uint32_t { return timeout; }
    auto timedOut() -> bool
    {
        if (timeout == 0) {
            return true;
        }
        --timeout;
        return false;
    }

protected:
    uint32_t timeout;
};

} // namespace remix::timeouts
