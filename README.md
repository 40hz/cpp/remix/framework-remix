# Remix PlatformIO Library


This is the [PlatformIO library](https://docs.platformio.org/en/latest/librarymanager/index.html)
component for Remix, to be used with the [Remix
platform](https://gitlab.com/40hz/cpp/remix/platform). It has these goals:

- Use modern C++ features
- Support different target platforms
- Provide a meaningful set of cross target drivers

The Remix library can also be used standalone without the Remix platform (see the `cmsis_platform`
example)

## Status

The Remix library is highly experimental and subject to change. The main focus is on ST Nucleo
boards, with other target hardware to follow (mainly MSP430 and AVR).
