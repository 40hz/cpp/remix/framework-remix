#include <array>
#include <span>

#include <remix.h>
#include <remix/drivers/nrf24.h>
#include <remix/timeouts.h>

#include "app_info.h"
#include "printer.h"

using namespace remix;

const drivers::nrf24::Driver<mcu::Spi, mcu::Pin> nrf24 {
    .spi = board::spi,
    .csn = board::csn,
    .ce = board::nrf24::ce,
};

static constexpr uint8_t txData[] = {
    0x00,
    0xff,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0xff,
    0x55,
};

static constexpr uint8_t moteInfo[] = {
    0xd8,
    0x31,
    0x81,
    0xd8,
    0x4a,
    0x81,
    0x47,
    0x30,
    0x31,
    0x32,
    0x33,
    0x34,
    0x35,
    0x36,
};

static constexpr uint8_t address[] = { 0x00, 0xf0, 0xf0, 0xf0, 0xf0 };

std::array<uint8_t, 64> packet;
printer::printer print(board::serial);

auto printRegisters() -> void
{
    for (auto r : nrf24.getRegisters()) {
        print(static_cast<uint8_t>(r.reg), printer::hex, printer::right, 2, '0');
        print(": ");
        print(static_cast<uint8_t>(r.value), printer::binary, printer::right, 8, '0');
        print.emit('\n');
    }
}

auto transmitTest() -> void
{
    print("Transmitting...\n");
    if (!nrf24.tx(std::span { txData }, timeouts::Counter(100))) {
        print("Error: Timeout\n");
    }
}

auto receiveTest() -> void
{
    print("Receiving...\n");
    while (true) {
        auto received = nrf24.rx(packet, timeouts::Counter(30000));
        if (received) {
            print.emit('\n');
            for (auto d : std::span(packet.begin(), received.value())) {
                print(d, printer::hex, printer::right, 2, '0');
            }
            print.emit('\n');
            break;
        }
        if (board::usart.canReceive()) {
            board::usart.receive();
            print.emit('\n');
            printRegisters();
            break;
        }
        print.emit('.');
    }
}

auto main() -> int
{
    board::init();
    board::led.init();
    board::led.set();
    board::tx.init();
    board::rx.init();
    board::nrf24::ce.init();
    board::csn.init();
    board::sck.init();
    board::sdo.init();
    board::sdi.init();
    board::usart.init();
    board::spi.init();
    board::csn.set();

    print("-- Start ------------------------------\n");
    print("   Git commit: ");
    print(commit);
    print("\nBuild time: ");
    print(build_time);
    print.emit('\n');
    nrf24.init();
    nrf24.setChannel(70);
    nrf24.setRxAddress(address);
    nrf24.setTxAddress(address);
    nrf24.txMode();
    while (true) {
        print("-- > ----------------------------------\nt)ransmit\nr)eceive\ns)tatus\n> ");

        while (!board::usart.canReceive())
            ;
        print.emit('\n');
        switch (board::usart.receive()) {
        case 't':
            transmitTest();
            break;
        case 'r':
            receiveTest();
            break;
        case 's':
            printRegisters();
        default:
            break;
        }
    }
    return 0;
}
