import subprocess
from datetime import datetime

sha = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).decode('ascii').strip()
date = datetime.utcnow().isoformat()
with open('include/app_info.h', 'w') as f:
	f.write('#pragma once\n\nconst char commit[] = "' + sha + '";\nconst char build_time[] = "' + date + 'Z";\n')
	f.close()
