#include "remix.h"

#include <remix/arch/arm/stm32/stm32f0.h>

namespace remix::board {

namespace nrf24 {
const mcu::Pin ce { .port = 2, .pin = 7, .mode = mcu::Pin::Output };
}
const mcu::Pin led { .port = 0, .pin = 0, .mode = mcu::Pin::Mode::Output };
const mcu::Pin button { .port = 1, .pin = 0, .mode = mcu::Pin::Mode::Output };
const mcu::Pin chip_select { .port = 2, .pin = 0, .mode = mcu::Pin::Mode::Output };
const mcu::Pin tx { .port = 0, .pin = 2, .mode = mcu::Pin::Alternate, .function = 1 };
const mcu::Pin rx { .port = 0, .pin = 3, .mode = mcu::Pin::Alternate, .function = 1 };
const mcu::Pin csn { .port = 1, .pin = 6, .mode = mcu::Pin::Output };
const mcu::Pin sck { .port = 0, .pin = 5, .mode = mcu::Pin::Alternate, .function = 5 };
const mcu::Pin sdi { .port = 0, .pin = 6, .mode = mcu::Pin::Alternate, .function = 5 };
const mcu::Pin sdo { .port = 0, .pin = 7, .mode = mcu::Pin::Alternate, .function = 5 };

const mcu::Usart usart { .instance = 1 };
const mcu::Spi spi { .instance = 0, .isMaster = true };

const mcu::Usart serial = usart;

auto init() -> void
{
    RCC->AHBENR = RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN | RCC_AHBENR_GPIOCEN | RCC_AHBENR_FLITFEN;
    RCC->APB1ENR = RCC_APB1ENR_USART2EN;
    RCC->APB2ENR = RCC_APB2ENR_SPI1EN;
}

}; // namespace remix::board

auto main() -> int
{
    remix::board::init();
    remix::board::led.init();
    while (true) {
        remix::board::led.toggle();
    }
    return 0;
}
