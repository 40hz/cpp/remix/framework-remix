#include "printer.h"
#include <span>

#include <remix.h>
#include <remix/timeouts.h>

using namespace remix;

static const uint8_t si7006 = 0b0100'0000;

auto main() -> int
{
    board::init();
    board::led.init();

    board::scl.init();
    board::sda.init();
    board::i2c.init();

    board::tx.init();
    board::rx.init();
    board::serial.init();
    printer::printer print(board::serial);

    while (true) {
        board::serial.write(std::span { "Press key to start...\n" });
        board::serial.receive();
        board::led.toggle();
        if (board::i2c.checkTarget(si7006, timeouts::Counter { 50'000 })) {
            board::serial.write(std::span { "Si7006 present.\n" });
            uint8_t buffer[2];
            auto result = std::span { buffer, 2 };
            if (board::i2c.readRegister(si7006, 0xe3, result, timeouts::Counter { 50'0000 })) {
                board::serial.write(std::span { "Result: " });
                print(buffer[0], printer::hex, printer::right, 2, '0');
                print(buffer[1], printer::hex, printer::right, 2, '0');
                board::serial.send(10);
            } else {
                board::serial.write(std::span { "Timeout reading temperature.\n" });
            }
        }
    }

    return 0;
}
