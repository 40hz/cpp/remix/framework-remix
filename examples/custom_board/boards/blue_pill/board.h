#pragma once

#include <remix/arch/arm/stm32/stm32f1.h>

namespace remix::board {

const mcu::Pin led { .port = 1, .pin = 8, .mode = mcu::Pin::Mode::Output };

auto init() -> void { RCC->APB2ENR = RCC_APB2ENR_IOPBEN; }

}; // namespace remix::board
