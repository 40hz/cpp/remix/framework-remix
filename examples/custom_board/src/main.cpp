#include "remix.h"

using namespace remix;

auto main() -> int
{
    board::init();
    board::led.init();
    while (true) {
        board::led.toggle();
    }
    return 0;
}
