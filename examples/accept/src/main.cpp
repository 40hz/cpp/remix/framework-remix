#include <span>

#include "remix.h"

#include "accept.h"

using namespace remix;

auto a = accept::Accept(board::serial);

auto main() -> int
{
    board::init();
    board::led.init();
    board::led.set();
    board::tx.init();
    board::rx.init();
    board::serial.init();

    board::serial.write(std::span { "\n---- Accept ------------------------\n" });
    bool editing = true;
    while (editing) {
        auto result = a.handle(board::serial.receive());
        switch (result) {
        case accept::Canceled:
            board::serial.write(std::span { "\nCanceled.\n" });
            editing = false;
            break;
        case accept::Accepted:
            board::serial.write(std::span { "\nDone: " });
            board::serial.write(a.accepted());
            board::serial.send('\n');
            editing = false;
            break;
        default:
            break;
        }
    }

    return 0;
}
