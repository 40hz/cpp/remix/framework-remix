#include <span>

#include "remix.h"

using namespace remix;

auto main() -> int
{
    board::init();
    board::led.init();
    board::led.set();
    board::tx.init();
    board::rx.init();
    board::serial.init();
    board::serial.write(std::span { "Press any key...\n" });
    while (true) {
        uint8_t key = board::serial.receive();
        board::serial.send(key);
        board::led.toggle();
    }

    return 0;
}
