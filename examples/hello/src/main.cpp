#include <span>

#include <remix.h>

using namespace remix;

auto main() -> int
{
    board::init();
    board::led.init();
    board::tx.init();
    board::rx.init();
    board::serial.init();
    while (true) {
        board::serial.write(std::span { "Hello, World! Press any key...\n" });
        board::led.toggle();
        board::serial.receive();
    }

    return 0;
}
