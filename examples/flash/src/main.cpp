#include <array>
#include <span>
#include <string>

#include <remix.h>

#include "accept.h"
#include "menu.h"

using namespace remix;

constexpr size_t maxInput = 18;

enum {
    Show_Settings,
    Input_Settings,
};

constexpr menu::Item showMenu { .title = "Show settings", .id = Show_Settings };
constexpr menu::Item inputMenu { .title = "Input settings", .id = Input_Settings };
constexpr std::array settingsItems { &showMenu, &inputMenu };
constexpr menu::Item settingsMenu {
    .title = "Settings",
    .action = menu::Item::Submenu,
    .submenu = settingsItems,
};

auto m = menu::State(settingsMenu, board::serial);
auto a = accept::Accept(board::serial);

struct Settings {
    uint8_t length;
    uint8_t data[maxInput];
    uint8_t checksum;

    auto calculateChecksum() -> uint8_t
    {
        uint8_t cs = length;
        for (auto c : data) {
            cs ^= c;
        }
        return cs;
    }

    auto check() -> bool { return length < maxInput && checksum == calculateChecksum(); }
} settings;

auto readSettings() -> void
{
    Settings* s = reinterpret_cast<Settings*>(board::infoSegment);
    if (!s->check()) {
        settings.length = 0;
        return;
    }
    settings = *s;
}

auto writeSettings(std::span<uint8_t> s) -> void
{
    settings.length = static_cast<uint8_t>(s.size());
    std::copy(s.begin(), s.end(), std::begin(settings.data));
    settings.checksum = settings.calculateChecksum();
    board::flash.unlock();
    board::flash.erase(board::infoSegment);
    board::flash.write(board::infoSegment, &settings);
    board::flash.lock();
}

auto handleInput(menu::Item const* const item) -> void
{
    a.reset();
    board::serial.write(item->title);
    board::serial.write(std::span { ": " });
    while (true) {
        auto result = a.handle(board::serial.receive());
        switch (result) {
        case accept::Canceled:
            board::serial.write(std::span { "\nCanceled.\n" });
            return;
        case accept::Accepted:
            board::serial.write(std::span { "\nSettings updated\n" });
            std::copy(a.accepted().begin(), a.accepted().end(), settings.data);
            return;
        default:
            break;
        }
    }
}

auto main() -> int
{
    board::flash.init();
    board::init();
    board::led.init();
    board::led.set();
    board::tx.init();
    board::rx.init();
    board::serial.init();

    readSettings();
    while (true) {
        m.display();
        auto r = m.handle(board::serial.receive());
        if (r) {
            switch (r.value()->id) {
            case Show_Settings:
                board::serial.write(std::span { "\nSettings: <" });
                board::serial.write(std::span { settings.data, settings.length });
                board::serial.write(std::span { ">\n" });
                break;
            case Input_Settings:
                handleInput(r.value());
                break;
            }
        }
    }
    return 0;
}
