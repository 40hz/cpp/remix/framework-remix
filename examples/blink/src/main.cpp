#include <remix.h>

using namespace remix;

auto main() -> int
{
    board::init();
    board::led.init();
    while (true) {
        board::led.toggle();
        for (uint32_t i = 0; i < 100'000; i = i + 1) {
            asm volatile("");
        }
    }
    return 0;
}
