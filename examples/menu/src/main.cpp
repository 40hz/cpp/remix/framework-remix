#include <array>
#include <span>
#include <string>

#include <remix.h>

#include "accept.h"
#include "menu.h"

using namespace remix;

constexpr size_t maxInput = 40;

enum {
    Show_Settings,
    Input_Settings,
    Led_On,
    Led_Off,
    Led_Toggle,
};

std::array<uint8_t, maxInput> settingsBuffer;
std::span<uint8_t> settings;

constexpr menu::Item showSettings { .title = "Show settings", .id = Show_Settings };
constexpr menu::Item inputSettings { .title = "Input settings", .id = Input_Settings };
constexpr std::array settingsItems { &showSettings, &inputSettings };
constexpr menu::Item settingsMenu {
    .title = "Settings",
    .action = menu::Item::Submenu,
    .submenu = settingsItems,
};

constexpr menu::Item ledOn { .title = "LED on", .id = Led_On };
constexpr menu::Item ledOff { .title = "LED off", .id = Led_Off };
constexpr menu::Item ledToggle { .title = "Toggle led", .id = Led_Toggle };
constexpr std::array toplevelItems { &ledOn, &ledOff, &ledToggle, &settingsMenu };
constexpr menu::Item toplevel {
    .title = "Menu",
    .action = menu::Item::Submenu,
    .submenu = toplevelItems,
};

auto m = menu::State(toplevel, board::serial);
auto a = accept::Accept(board::serial);

auto handleInput(menu::Item const* const item) -> void
{
    a.reset();
    board::serial.write(item->title);
    board::serial.write(std::span { ": " });
    while (true) {
        auto result = a.handle(board::serial.receive());
        switch (result) {
        case accept::Canceled:
            board::serial.write(std::span { "\nCanceled.\n" });
            return;
        case accept::Accepted:
            board::serial.write(std::span { "\nSettings updated\n" });
            return;
        default:
            break;
        }
    }

    auto input = a.accepted();
    std::copy(input.begin(), input.end(), settingsBuffer.begin());
    settings = std::span { settingsBuffer.begin(), input.size() };
}

auto main() -> int
{
    board::init();
    board::led.init();
    board::led.set();
    board::tx.init();
    board::rx.init();
    board::serial.init();

    while (true) {
        m.display();
        auto r = m.handle(board::serial.receive());
        if (r) {
            switch (r.value()->id) {
            case Input_Settings:
                handleInput(r.value());
                break;
            case Show_Settings:
                board::serial.write(std::span { "\nSettings: " });
                board::serial.write(settings);
                board::serial.send('\n');
                break;
            case Led_On:
                board::led.set();
                break;
            case Led_Off:
                board::led.clear();
                break;
            case Led_Toggle:
                board::led.toggle();
                break;
            }
        }
    }
    return 0;
}
